﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Auth.Quickstart.Account
{
    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Interfaces;
using Llano.Models;
using Llano.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Llano.Auth.Quickstart.Account
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<Customer> _userManager;
        private IUserServices _userServices;

        public UserController(
            UserManager<Customer> userManager,
             IUserServices userServices
            )
        {
            _userManager = userManager;
            this._userServices = userServices;
        }

        [HttpGet]
        [Route("getByEmail/{email}")]
        public async Task<ActionResult<Customer>> GetByEmail(string email)
        {
            var customer = await this._userManager.FindByEmailAsync(email);
            return Ok(customer);
        }

        [HttpGet]
        [Route("getUser/{idUser}")]
        public async Task<ActionResult<UserViewModel>> GetUser(string idUser)
        {
            return await _userServices.GetUser(idUser);
        }


    }
}
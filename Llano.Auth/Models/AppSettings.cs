﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Auth.Models
{
    public class AppSettings
    {
        public string BackendUrl { get; set; }
    } 
}

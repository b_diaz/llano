﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Auth.Models
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public RegisterResponseViewModel(Customer user)
        {
            Id = user.Id;
            Name = user.FirstName;
            Email = user.Email;
        }
    }

    public class RegisterRequestViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "El {0} debe tener entre {2} y {1} Caracteres..", MinimumLength = 2)]
        [Display(Name = "Nombres")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El {0} debe tener entre {2} y {1} Caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password, ErrorMessage = "La contraseña no cumple con los criterios")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "El {0} debe tener entre {2} y {1} Caracteres..", MinimumLength = 2)]
        [Display(Name = "Apellidos")]
        public string LastName { get; set; }
        [Required]
        public string DocumentTypeId { get; set; }
        [Required]
        public string DocumentId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public bool AllowMobileNotifications { get; set; }
        public bool AllowEmailNotifications { get; set; }
        public bool AllowSMSAndCalls { get; set; }
    }
}

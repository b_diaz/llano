﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using Llano.Auth;
using Llano.Auth.Models;
using Llano.Auth.Services;
using Llano.DataAccess;
using Llano.Interfaces;
using Llano.Models;
using Llano.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Llano.Auth
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {



            #region ---------- Configuracion Cors ----------            
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                   // builder.WithOrigins("http://localhost:8100", "https://localhost:44320").AllowAnyHeader().AllowAnyMethod().AllowCredentials();


                });
            });
            #endregion

            Configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.Development.json", optional: true)
                        .AddEnvironmentVariables()
                    .Build();

            #region -------- Conexion base de datos --------

            services.AddDbContext<LlanoDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            #endregion

            services.AddIdentity<Customer, IdentityRole>(config =>
            {
                config.SignIn.RequireConfirmedEmail = false;
                config.User.RequireUniqueEmail = true;
                config.Password.RequireDigit = false;
                config.Password.RequiredLength = 6;
                config.Password.RequireLowercase = false;
                config.Password.RequireUppercase = false;
                config.Password.RequireNonAlphanumeric = false;
            })
            .AddEntityFrameworkStores<LlanoDbContext>()
            .AddDefaultTokenProviders();

            services.Configure<AppSettings>(Configuration.GetSection("AppsSettings"));


            var settings = Configuration.GetSection("AppsSettings").Get<AppSettings>();

            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient<IBackendService, BackendService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IPathProvider, PathProvider>();
            services.AddTransient<IUserServices, UserServices>();


            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 3600; // interval in seconds

                }).AddInMemoryPersistedGrants()
                .AddInMemoryIdentityResources(Config.GetIdentityResources(settings))
                .AddInMemoryApiResources(Config.GetApis(settings))
                .AddInMemoryClients(Config.GetClients(settings))
                .AddAspNetIdentity<Customer>()
                .AddProfileService<ProfileService>();


            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    // register your IdentityServer with Google at https://console.developers.google.com
                    // enable the Google+ API
                    // set the redirect URI to http://localhost:5000/signin-google
                    options.ClientId = Configuration["Authentication:Google:ClientId"];
                    options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                });

            #region --------------- Serializando Json ---------------
            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
            #endregion


        }

        public void Configure(IApplicationBuilder app)
        {
          if (Environment.IsDevelopment())
          {
              app.UseDeveloperExceptionPage();
              app.UseDatabaseErrorPage();
          }
            else
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                app.UseHsts();
                app.UseExceptionHandler("/Home/Error");
                app.UseHttpsRedirection();

                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                // app.UseForwardedHeaders(new ForwardedHeadersOptions
                // {
                //     ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                // });
            }

            app.UseCors();
            app.UseStaticFiles();
            app.UseIdentityServer();
            app.UseMvcWithDefaultRoute();
        }
    }
}
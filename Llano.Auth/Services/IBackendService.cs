﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Auth.Services
{
    public interface IBackendService
    {
        Task<Customer> GetCustomerByEmail(string email);
        Task<bool> AddCustomer(Customer customer);
    }
}

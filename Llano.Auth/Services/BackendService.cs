﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using IdentityServer4;
using Llano.Auth.Models;
using Llano.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Llano.Auth.Services
{
    public class BackendService : IBackendService
    {
        private readonly IdentityServerTools _identityServerTools;
        private readonly AppSettings _appSettings;
        public BackendService(IdentityServerTools tools, IOptions<AppSettings> appsSettingsOptions)
        {
            _identityServerTools = tools;
            _appSettings = appsSettingsOptions.Value;
        }

        public async Task<bool> AddCustomer(Customer customer)
        {
            string token = await GetToken();
            var client = new HttpClient();
            client.SetBearerToken(token);
            var content = new StringContent(JsonConvert.SerializeObject(customer));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync($"{_appSettings.BackendUrl}/customer/", content);
            return await response.Content.ReadAsAsync<bool>();
        }

        public async Task<Customer> GetCustomerByEmail(string email)
        {
            string token = await GetToken();

            var client = new HttpClient();
            client.SetBearerToken(token);

            var response = await client.GetAsync($"{_appSettings.BackendUrl}/customer/getByEmail/{email}/");

            return await response.Content.ReadAsAsync<Customer>();
        }

        private async Task<string> GetToken()
        {
            return await _identityServerTools.IssueClientJwtAsync(
              clientId: "client",
              lifetime: 3600,
              scopes: new[] { "api.read" },
              audiences: new[] { "webapi.eaaay.gov.co" });
        }
    }
}

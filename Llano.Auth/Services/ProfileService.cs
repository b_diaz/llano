﻿using Llano.Auth.Models;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Llano.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Auth.Services
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<Customer> _userManager;

        public ProfileService(UserManager<Customer> userManager)
        {
            _userManager = userManager;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            context.IssuedClaims.AddRange(context.Subject.Claims);

            return Task.FromResult(0);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.FromResult(0);
        }
    }
}

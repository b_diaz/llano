﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Llano.Auth
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources(Models.AppSettings settings)
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile(),
                new IdentityResources.Phone(),
                new IdentityResource( "roles","Roles del Usuario", new [] { "role" } )
            };
        }

        public static IEnumerable<ApiResource> GetApis(Models.AppSettings settings)
        {
            return new ApiResource[]
            {
                new ApiResource("webapi.LlanoApp.com.co", "Web Api LlanoApp")
                {
                    UserClaims=
                    {
                        JwtClaimTypes.Email,
                        JwtClaimTypes.Name,
                        "roles",
                        JwtClaimTypes.GivenName,
                        JwtClaimTypes.FamilyName,
                        JwtClaimTypes.PhoneNumber
                    },
                    Scopes = {new Scope("api.read","Acceso a la información de la LlanoApp")}
                }
            };
        }

        public static IEnumerable<Client> GetClients(Models.AppSettings settings)
        {
            return new[]
            {
                // resource owner password grant client
                new Client
                {
                    ClientId = "mobile.LlanoApp.com.co",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowOfflineAccess=true,
                    AlwaysSendClientClaims = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "openid", "profile","phone", "email", "api.read","offline_access","roles" },
                },
               
            };
        }
    }
}
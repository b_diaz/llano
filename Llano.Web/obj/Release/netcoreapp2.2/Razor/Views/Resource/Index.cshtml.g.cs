#pragma checksum "C:\llano\Llano.Web\Views\Resource\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "23a96b863cf6c2f2a619c3effc2b0ec31f6dd976"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Resource_Index), @"mvc.1.0.view", @"/Views/Resource/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Resource/Index.cshtml", typeof(AspNetCore.Views_Resource_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\llano\Llano.Web\Views\_ViewImports.cshtml"
using Llano.Web.Models;

#line default
#line hidden
#line 2 "C:\llano\Llano.Web\Views\_ViewImports.cshtml"
using Llano.Models;

#line default
#line hidden
#line 3 "C:\llano\Llano.Web\Views\_ViewImports.cshtml"
using Llano.ViewModels;

#line default
#line hidden
#line 4 "C:\llano\Llano.Web\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"23a96b863cf6c2f2a619c3effc2b0ec31f6dd976", @"/Views/Resource/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"391378835ad9dd58ed497abc89365768dfd617d0", @"/Views/_ViewImports.cshtml")]
    public class Views_Resource_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Resource>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("d-flex text-center  align-items-center link-muted py-2 px-3"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Resource", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Read", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(23, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
   
    ViewData["Title"] = "Recursos";

#line default
#line hidden
            BeginContext(70, 41, true);
            WriteLiteral("\r\n<main class=\"u-main\" role=\"main\">\r\n    ");
            EndContext();
            BeginContext(112, 38, false);
#line 8 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
Write(await Component.InvokeAsync("Sidebar"));

#line default
#line hidden
            EndContext();
            BeginContext(150, 330, true);
            WriteLiteral(@"
    <div class=""u-content"">
        <div class=""u-body"">

            <!--  Tabla para mostrar los contenidos en estado solicitado -->
            <div class=""card"">
                <div class=""card mb-4"">
                    <header class=""card-header"">
                        <h2 class=""h3 card-header-title"">Lista de ");
            EndContext();
            BeginContext(481, 27, false);
#line 16 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                             Write(TempData["Type"].ToString());

#line default
#line hidden
            EndContext();
            BeginContext(508, 141, true);
            WriteLiteral("</h2>\r\n                    </header>\r\n                    <div class=\"card-body\">\r\n                        <div class=\"table-responsive\">\r\n\r\n");
            EndContext();
#line 21 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                             if (Model.Count > 0)
                            {

#line default
#line hidden
            BeginContext(731, 948, true);
            WriteLiteral(@"                                <table class=""table table-hover"">
                                    <thead>
                                        <tr>
                                            <th scope=""col"">#</th>
                                            <th scope=""col"">Nombre</th>
                                            <th scope=""col"">Solicita</th>
                                            <th scope=""col"">Correo</th>
                                            <th scope=""col"">Categoria</th>
                                            <th scope=""col"" width=""180"">Fecha solicitud</th>
                                            <th class=""text-center"" scope=""col"">
                                                Accion
                                            </th>

                                        </tr>
                                    </thead>

                                    <tbody>
");
            EndContext();
#line 40 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                          
                                            var index = 1;
                                            foreach (var resource in Model)
                                            {
                                                



#line default
#line hidden
            BeginContext(1961, 110, true);
            WriteLiteral("                                                <tr>\r\n                                                    <td>");
            EndContext();
            BeginContext(2072, 5, false);
#line 48 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                   Write(index);

#line default
#line hidden
            EndContext();
            BeginContext(2077, 63, true);
            WriteLiteral("</td>\r\n                                                    <td>");
            EndContext();
            BeginContext(2141, 13, false);
#line 49 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                   Write(resource.Name);

#line default
#line hidden
            EndContext();
            BeginContext(2154, 64, true);
            WriteLiteral(" </td>\r\n                                                    <td>");
            EndContext();
            BeginContext(2219, 34, false);
#line 50 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                   Write(resource.UserApplication.FirstName);

#line default
#line hidden
            EndContext();
            BeginContext(2253, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(2255, 33, false);
#line 50 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                       Write(resource.UserApplication.LastName);

#line default
#line hidden
            EndContext();
            BeginContext(2288, 64, true);
            WriteLiteral(" </td>\r\n                                                    <td>");
            EndContext();
            BeginContext(2353, 30, false);
#line 51 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                   Write(resource.UserApplication.Email);

#line default
#line hidden
            EndContext();
            BeginContext(2383, 68, true);
            WriteLiteral(" </td>\r\n                                                    <td>\r\n\r\n");
            EndContext();
#line 54 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                         switch (resource.TypeResource.Name)
                                                        {

                                                            case "Palabras":

#line default
#line hidden
            BeginContext(2684, 109, true);
            WriteLiteral("                                                                <a class=\"badge badge-soft-success\" href=\"#\">");
            EndContext();
            BeginContext(2794, 26, false);
#line 58 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                                        Write(resource.TypeResource.Name);

#line default
#line hidden
            EndContext();
            BeginContext(2820, 6, true);
            WriteLiteral("</a>\r\n");
            EndContext();
#line 59 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                break;
                                                            case "Refranes":

#line default
#line hidden
            BeginContext(2976, 106, true);
            WriteLiteral("                                                                <a class=\"badge badge-soft-info\" href=\"#\">");
            EndContext();
            BeginContext(3083, 26, false);
#line 61 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                                     Write(resource.TypeResource.Name);

#line default
#line hidden
            EndContext();
            BeginContext(3109, 6, true);
            WriteLiteral("</a>\r\n");
            EndContext();
#line 62 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                break;
                                                            case "Coplas":

#line default
#line hidden
            BeginContext(3263, 109, true);
            WriteLiteral("                                                                <a class=\"badge badge-soft-warning\" href=\"#\">");
            EndContext();
            BeginContext(3373, 26, false);
#line 64 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                                        Write(resource.TypeResource.Name);

#line default
#line hidden
            EndContext();
            BeginContext(3399, 6, true);
            WriteLiteral("</a>\r\n");
            EndContext();
#line 65 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                break;
                                                            case "Leyendas":

#line default
#line hidden
            BeginContext(3555, 108, true);
            WriteLiteral("                                                                <a class=\"badge badge-soft-danger\" href=\"#\">");
            EndContext();
            BeginContext(3664, 26, false);
#line 67 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                                       Write(resource.TypeResource.Name);

#line default
#line hidden
            EndContext();
            BeginContext(3690, 6, true);
            WriteLiteral("</a>\r\n");
            EndContext();
#line 68 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                break;
                                                            default:
                                                                break;
                                                        }

#line default
#line hidden
            BeginContext(3969, 115, true);
            WriteLiteral("                                                    </td>\r\n                                                    <td>");
            EndContext();
            BeginContext(4085, 38, false);
#line 73 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                   Write(resource.CreatedOn.ToShortDateString());

#line default
#line hidden
            EndContext();
            BeginContext(4123, 142, true);
            WriteLiteral(" </td>\r\n                                                    <td class=\"text-center\">\r\n                                                        ");
            EndContext();
            BeginContext(4265, 319, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "23a96b863cf6c2f2a619c3effc2b0ec31f6dd97614271", async() => {
                BeginContext(4416, 164, true);
                WriteLiteral("\r\n                                                            <i class=\"fa fa-external-link-alt mr-2\"></i>\r\n                                                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-idResource", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 75 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                                                                                                                                                                                     WriteLiteral(resource.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idResource"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-idResource", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idResource"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4584, 116, true);
            WriteLiteral("\r\n                                                    </td>\r\n                                                </tr>\r\n");
            EndContext();
#line 80 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"

                                                index = index + 1;
                                            }
                                        

#line default
#line hidden
            BeginContext(4860, 90, true);
            WriteLiteral("\r\n                                    </tbody>\r\n                                </table>\r\n");
            EndContext();
#line 87 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                            }
                            else
                            {

#line default
#line hidden
            BeginContext(5046, 66, true);
            WriteLiteral("                                <p>No se ha creado contenido</p>\r\n");
            EndContext();
#line 91 "C:\llano\Llano.Web\Views\Resource\Index.cshtml"
                            }

#line default
#line hidden
            BeginContext(5143, 155, true);
            WriteLiteral("\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</main>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Resource>> Html { get; private set; }
    }
}
#pragma warning restore 1591

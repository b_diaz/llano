﻿using Llano.Interfaces;
using Llano.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Web.ViewComponents
{
    public class HeaderViewComponent: ViewComponent
    {
        #region ---------------- Dependencias  ---------------

        private readonly IResourceService _resourceService;

        public HeaderViewComponent
            (
            IResourceService resourceService
            )
        {
            this._resourceService = resourceService;
        }

        #endregion


        public async Task<IViewComponentResult> InvokeAsync(HeaderViewModel model)
        {
            var notifications =  await this._resourceService.ResourcesWithoutRead();

            var newModel = new HeaderViewModel()
            {
                Notifications = notifications
            };

            return View(newModel);
        }

    }
}

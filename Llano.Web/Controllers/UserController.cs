﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Interfaces;
using Llano.Models;
using Llano.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Llano.Web.Controllers
{
    public class UserController : Controller
    {

        #region ---------------- Injecccion de dependencias -----------------

        private readonly UserManager<Customer> _userManager;
        private readonly IResourceService _resourceServices;
        private readonly IResourceService _resourceService;
        private readonly IFcmService _fcmService;

        public UserController(
            UserManager<Customer> userManager,
            IResourceService resourceServices,
              IResourceService resourceService,
              IFcmService fcmService
            )
        {
            _userManager = userManager;
            _resourceServices = resourceServices;
            this._resourceService = resourceService;
            this._fcmService = fcmService;
        }

        #endregion

        public async Task<IActionResult> Index()
        {
            var users = await this._userManager.Users.ToListAsync();

            //esto evita que se muestre el usuario administrador
            var usersWithOutAdmin = users.Where(x => x.PhoneNumber != null).ToList();

            return View(usersWithOutAdmin);
        }

        public async Task<IActionResult> Read(string UserId)
        {
            var user = await this._userManager.FindByIdAsync(UserId);
            var resources = await this._resourceServices.GetForUserId(UserId);

            CustomerMessageViewModels CustomerMessageViewModels = new CustomerMessageViewModels
            {
                UserApplication = user,
                Resources = resources.ToList()
            };

            return View(CustomerMessageViewModels);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(CustomerMessageViewModels CustomerMessageViewModels)
        {
            try
            {
                var message = CustomerMessageViewModels.Message;
                var user = await this._userManager.FindByIdAsync(CustomerMessageViewModels.UserApplication.Id);
               

                await this._userManager.UpdateAsync(user);

                //busco si tiene recursos 

                var resourcesForThisUser =  await this._resourceService.GetForUserId(user.Id);

                foreach (var resource in resourcesForThisUser)
                {
                    await this._resourceService.Delete(resource.Id);
                }

                var userDelete = await this._userManager.DeleteAsync(user);

                var notification = new Notification
                {
                    Title = "Usuario Eliminado",
                    Body = message,
                    CustomerId = user.Id,
                    Type = "NotificationUser"

                };

                await this._fcmService.SendNotification(notification);





                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
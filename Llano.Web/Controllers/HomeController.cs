﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Llano.Web.Models;
using Llano.Interfaces;
using Llano.Models;
using Microsoft.AspNetCore.Authorization;

namespace Llano.Web.Controllers
{
    public class HomeController : Controller
    {
        #region ------------ Dependencias --------------

        private readonly IResourceService _resourceService;

        public HomeController
            (
            IResourceService resourceService
            )
        {
            this._resourceService = resourceService;
        }

        #endregion

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            // Solicitado = 1
            // Descartado = 2
            // Aprobado = 3


            // Palabras = 1
            // Refranes = 2
            // Coplas = 3
            // Leyendas = 4


            List<Resource> resourcesInStatusSolicitado = await this._resourceService.GetForSatus(1);

            List<Resource> Palabras = await this._resourceService.GetForType(1);
            List<Resource> Refranes = await this._resourceService.GetForType(2);
            List<Resource> Coplas = await this._resourceService.GetForType(3);
            List<Resource> Leyendas = await this._resourceService.GetForType(4);

            TempData["Palabras"] = Palabras.Count;
            TempData["Refranes"] = Refranes.Count;
            TempData["Coplas"] = Coplas.Count;
            TempData["Leyendas"] = Leyendas.Count;

            return View(resourcesInStatusSolicitado);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

       
    }
}

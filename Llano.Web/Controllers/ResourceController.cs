﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Interfaces;
using Llano.Models;
using Llano.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Llano.Web.Controllers
{
    public class ResourceController : Controller
    {

        #region ---------------- Dependencias  ---------------
        private readonly IResourceService _resourceService;
        private readonly IHistoryResource _historyService;
        private readonly IFcmService _fcmService;

        public ResourceController
            (
            IResourceService resourceService,
            IHistoryResource historyService,
            IFcmService fcmService
            )
        {
            this._resourceService = resourceService;
            this._historyService = historyService;
            this._fcmService = fcmService;
        }

        #endregion

        
        public async Task<IActionResult> Index(int idTypeResource)
        {
            switch (idTypeResource)
            {
                case 1:
                    TempData["Type"] = "Palabras";
                    break;
                case 2:
                    TempData["Type"] = "Refranes";
                    break;
                case 3:
                    TempData["Type"] = "Coplas";
                    break;
                case 4:
                    TempData["Type"] = "Leyendas";
                    break;

                default:
                    TempData["Type"] = "Aportes sin Leer";
                    break;
            }

            // Notificaciones = 5
            if (idTypeResource == 5)
            {
                var notifications = await this._resourceService.ResourcesWithoutRead();
                return View(notifications);
            }
            else
            {
                var resourcesTypeSelect = await this._resourceService.GetForType(idTypeResource);
                return View(resourcesTypeSelect);
            }

        }

        public async Task<IActionResult> Read(long idResource)
        {
            Resource resource = await this._resourceService.GetForId(idResource);

            List<HistoryResource> hitoriesResource = await this._resourceService.HistoryResource(idResource);

            var model = new ResourceHistoriesViewModel()
            {
                Resource = resource,
                HistoriesResource = hitoriesResource
            };

            //El recurso se marca como leido
            await this._resourceService.MarkAsRead(idResource);

            return View(model);
        }

        public async Task<IActionResult> Edit(long idResource)
        {
            Resource resource = await this._resourceService.GetForId(idResource);

            List<HistoryResource> hitoriesResource = await this._resourceService.HistoryResource(idResource);

            var model = new ResourceHistoriesViewModel()
            {
                Resource = resource,
                HistoriesResource = hitoriesResource
            };


            return View(model);
        }

        public IActionResult Logout()
        {
            return SignOut("Cookies", "oidc");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ResourceHistoriesViewModel resourceHistories)
        {
            try
            {
                // Solicitado = 1
                // Descartado = 2
                // Aprobado = 3

                if (resourceHistories.Resource.Disable)
                {
                    resourceHistories.Resource.StatusResourceId = 2;
                }
                else
                {
                    resourceHistories.Resource.StatusResourceId = 3;
                }

                await this._resourceService.Update(resourceHistories.Resource.Id, resourceHistories.Resource);


                //creando historial de recurso
                var resource = await this._resourceService.GetForId(resourceHistories.Resource.Id);
                HistoryResource historyResource = new HistoryResource()
                {
                    StatusResourceId = resource.StatusResourceId,
                    ResourceId = resource.Id,
                    UserApplicationId = resource.UserApplicationId,
                    Message = resourceHistories.Message

                };

                this._historyService.CreateHistoryResource(historyResource);
                var notification = new Notification
                {
                    Title = "Cambio de Estado",
                    Body = historyResource.Message + ". - Nombre del Aporte: " + resource.Name,
                    CustomerId = historyResource.UserApplicationId,
                    Type = "Resource",
                    TypeId = resourceHistories.Resource.TypeResourceId,
                    ResourceId = resourceHistories.Resource.Id                  

                };
                switch (resourceHistories.Resource.StatusResourceId)
                {
                    case 2:
                        notification.Title = "Solicitud de aporte Descartado";
                        break;
                    case 3:
                        notification.Title = "Solicitud de aporte Aprobado";
                        break;
                    default:
                        break;
                }

                await this._fcmService.SendNotification(notification);

                return RedirectToAction("Index", new { idTypeResource = resourceHistories.Resource.TypeResourceId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}
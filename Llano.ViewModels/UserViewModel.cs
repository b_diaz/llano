﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.ViewModels
{
    public class UserViewModel
    {
        public Customer UserApplication { get; set; }
        public int Requested { get; set; }
        public int Approved { get; set; }
        public int Discarded { get; set; }
    }
}

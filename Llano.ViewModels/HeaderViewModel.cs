﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.ViewModels
{
    public class HeaderViewModel
    {
        public List<Resource> Notifications { get; set; }
    }
}

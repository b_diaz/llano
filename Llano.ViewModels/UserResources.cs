﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.ViewModels
{
    public class UserResources
    {
        public Customer UserApplication { get; set; }
        public List<Resource> Resources { get; set; }
    }
}

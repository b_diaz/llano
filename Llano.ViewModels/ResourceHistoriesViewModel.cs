﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.ViewModels
{
    public class ResourceHistoriesViewModel
    {
        public Resource Resource { get; set; }
        public List<HistoryResource> HistoriesResource { get; set; }
        public string Message { get; set; }

    }
}

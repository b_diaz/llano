﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.ViewModels
{
    public class MessageViewModel
    {
        public Notification Notification { get; set; }
        public CustomerData Data { get; set; }
        public string To { get; set; }
    }

    public class CustomerData
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public long ResourceId { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
    }
}

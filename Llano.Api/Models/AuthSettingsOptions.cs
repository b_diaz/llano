﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Api.Models
{
    public class AuthSettingsOptions
    {
        public string AuthServer { get; set; }
    }
}

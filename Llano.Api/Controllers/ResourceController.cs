﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Interfaces;
using Llano.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Llano.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class ResourceController : ControllerBase
    {
        #region  ------------ Dependencias -------------
        readonly IResourceService _resourceService;
        readonly IMediaServices _mediaServices;
        readonly IHistoryResource _historyResource;

        public ResourceController
            (
            IResourceService resourceService,
            IMediaServices mediaServices,
            IHistoryResource historyResource
            )
        {
            this._resourceService = resourceService;
            this._mediaServices = mediaServices;
            this._historyResource = historyResource;
        }
        #endregion

        #region --------- GET ---------
        // GET: api/Resource
        [HttpGet]
        public async Task<List<Resource>> Get()
        {
            return await this._resourceService.GetAll();
        }

        // GET: api/Resource/{id}
        [HttpGet]
        [Route("{ResourceId}")]
        public async Task<Resource> Get(long ResourceId)
        {
            return await this._resourceService.GetForId(ResourceId);
        }

        // GET: api/Resource/Type/{typeResourceId}
        [HttpGet]
        [Route("Type/{typeResourceId}")]
        public async Task<List<Resource>> Get(int typeResourceId)
        {
            return await this._resourceService.GetForType(typeResourceId);
        }

        // GET: api/Resource/Type/Contain/{typeResourceId}/{contain}
        [HttpGet("Type/Contain/{typeResourceId}/{contain}")]
        public async Task<List<Resource>> Get(int typeResourceId, string contain)
        {
            return await this._resourceService.GetForTypeAndContain(typeResourceId, contain);
        }

        // GET: api/Resource/User/{id}
        [HttpGet]
        [Route("User/{UserApplicationId}")]
        public async Task<List<Resource>> Get(string UserApplicationId)
        {
            return await this._resourceService.GetForUserId(UserApplicationId);
        }

        // GET: api/Resource/User/Type/{id}/{Type}
        [HttpGet]
        [Route("User/Type/Status/{UserApplicationId}/{TypeResourceId}/{StatusResourceId}")]
        public async Task<List<Resource>> Get(string UserApplicationId, int TypeResourceId, int StatusResourceId)
        {
            return await this._resourceService.GetForUserAndTypeAndStatus(UserApplicationId, TypeResourceId, StatusResourceId);
        }
        #endregion

        #region --------------- POST ---------------
        [Authorize]
        [HttpPost]      
        public async Task<IActionResult> New()
        {
            //    Solicitado = 1,
            //    Descartado = 2,
            //    Aprobado = 3

            //Creado recurso
            try
            {
                var formResource = Request.Form;

                var resource = new Resource()
                {
                    Name = formResource["Name"],
                    Content = formResource["Content"],
                    Read = false,
                    Disable = false,

                    TypeResourceId = int.Parse(formResource["TypeResourceId"]),
                    UserApplicationId = formResource["UserApplicationId"],
                    StatusResourceId = 1

                };

                if (Request.Form.Files.Any() && Request.Form.Files[0].Length > 0)
                {

                    IFormFile file = Request.Form.Files[0];
                    string storedFilePath = await _mediaServices.SaveFile(file.OpenReadStream(), resource.UserApplicationId, file.FileName);

                    resource.Audio = storedFilePath;

                }

                this._resourceService.CreateResource(resource);

                //creando historial de recurso
                HistoryResource historyResource = new HistoryResource()
                {
                    StatusResourceId = resource.StatusResourceId,
                    ResourceId = resource.Id,
                    UserApplicationId = resource.UserApplicationId
                };

                this._historyResource.CreateHistoryResource(historyResource);
                
                var resources = await this._resourceService.GetForUserId(resource.UserApplicationId);

                return Ok(resources);

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        #endregion


        // PUT: api/Resource/5
        [HttpPut("{id}")]
        [Authorize]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize]
        public void Delete(int id)
        {
        }

    }

}

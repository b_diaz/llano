﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Llano.Interfaces;
using Llano.Models;
using Llano.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Llano.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region  ------------ Dependencias -------------
        private IUserServices _userServices;

        public UserController
            (
            IUserServices userServices
            )
        {
            this._userServices = userServices;
        }
        #endregion

        #region ------------- GET -------------
        // GET: api/User/{id}
        [HttpGet]
        [Route("{userId}")]
        public async Task<UserViewModel> Get(string userId)
        {
            return await this._userServices.GetUser(userId);
        }


        [HttpGet]
        [Route("getByEmail/{email}")]
        public async Task<ActionResult<Customer>> GetByEmail(string email)
        {
            var model = await this._userServices.GetUserByEmail(email);
            return Ok(model);
        }

        #endregion


    }
}
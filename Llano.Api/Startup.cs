﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Api.Models;
using Llano.DataAccess;
using Llano.Interfaces;
using Llano.Models;
using Llano.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Llano.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            #region ---------- Configuracion Cors ----------            
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                builder =>
                {
                     builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                   // builder.WithOrigins("http://localhost:8100", "https://localhost:44320").AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                });
            });
            #endregion

            #region -------- Conexion base de datos --------

            services.AddDbContext<LlanoDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDefaultIdentity<LlanoDbContext>()
                 .AddRoles<IdentityRole>();

             
            #endregion


            #region --------------- Services ------------
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddTransient<IResourceService, ResourceServices>();
            services.AddTransient<IMediaServices, MediaServices>();
            services.AddTransient<IHistoryResource, HistoryResourceServices>();
            services.AddTransient<IUserServices, UserServices>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddSingleton<IPathProvider, PathProvider>();

            #endregion



            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });



            #region --------------- Serializando Json ---------------
            services.AddMvcCore().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
               .AddAuthorization()
               .AddJsonFormatters()
               .AddJsonOptions(options =>
               {
                   options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
               });
            #endregion

            var authSettings = new AuthSettingsOptions();
            Configuration.GetSection("AuthSettings").Bind(authSettings);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.Authority = authSettings.AuthServer;
                    options.RequireHttpsMetadata = false;// HostingEnvironment.IsProduction();
                   options.Audience = "webapi.LlanoApp.com.co";
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

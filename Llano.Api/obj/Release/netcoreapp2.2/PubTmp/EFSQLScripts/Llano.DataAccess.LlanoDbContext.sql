﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [UserName] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [Email] nvarchar(256) NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [PasswordHash] nvarchar(max) NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [LockoutEnabled] bit NOT NULL,
        [AccessFailedCount] int NOT NULL,
        [FirstName] nvarchar(max) NULL,
        [LastName] nvarchar(max) NULL,
        [AllowMobileNotifications] bit NOT NULL,
        [AllowEmailNotifications] bit NOT NULL,
        [CreatedOn] datetime2 NOT NULL,
        [UpdatedOn] datetime2 NOT NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [StatusResource] (
        [Id] int NOT NULL IDENTITY,
        [CreatedOn] datetime2 NOT NULL,
        [UpdatedOn] datetime2 NOT NULL,
        [Name] nvarchar(max) NULL,
        CONSTRAINT [PK_StatusResource] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [TypeResource] (
        [Id] int NOT NULL IDENTITY,
        [CreatedOn] datetime2 NOT NULL,
        [UpdatedOn] datetime2 NOT NULL,
        [Name] nvarchar(max) NULL,
        CONSTRAINT [PK_TypeResource] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [RoleId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [UserId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(128) NOT NULL,
        [ProviderKey] nvarchar(128) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(128) NOT NULL,
        [Name] nvarchar(128) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [Resource] (
        [Id] bigint NOT NULL IDENTITY,
        [CreatedOn] datetime2 NOT NULL,
        [UpdatedOn] datetime2 NOT NULL,
        [Name] nvarchar(max) NULL,
        [Content] nvarchar(max) NULL,
        [Audio] nvarchar(max) NULL,
        [Read] bit NOT NULL,
        [Disable] bit NOT NULL,
        [TypeResourceId] int NOT NULL,
        [StatusResourceId] int NOT NULL,
        [UserApplicationId] nvarchar(450) NULL,
        CONSTRAINT [PK_Resource] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Resource_StatusResource_StatusResourceId] FOREIGN KEY ([StatusResourceId]) REFERENCES [StatusResource] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Resource_TypeResource_TypeResourceId] FOREIGN KEY ([TypeResourceId]) REFERENCES [TypeResource] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Resource_AspNetUsers_UserApplicationId] FOREIGN KEY ([UserApplicationId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE TABLE [HistoryResource] (
        [Id] bigint NOT NULL IDENTITY,
        [CreatedOn] datetime2 NOT NULL,
        [UpdatedOn] datetime2 NOT NULL,
        [Message] nvarchar(max) NULL,
        [StatusResourceId] int NOT NULL,
        [ResourceId] bigint NOT NULL,
        [UserApplicationId] nvarchar(450) NULL,
        CONSTRAINT [PK_HistoryResource] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_HistoryResource_Resource_ResourceId] FOREIGN KEY ([ResourceId]) REFERENCES [Resource] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_HistoryResource_StatusResource_StatusResourceId] FOREIGN KEY ([StatusResourceId]) REFERENCES [StatusResource] ([Id]),
        CONSTRAINT [FK_HistoryResource_AspNetUsers_UserApplicationId] FOREIGN KEY ([UserApplicationId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'AllowEmailNotifications', N'AllowMobileNotifications', N'ConcurrencyStamp', N'CreatedOn', N'Email', N'EmailConfirmed', N'FirstName', N'LastName', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UpdatedOn', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
        SET IDENTITY_INSERT [AspNetUsers] ON;
    INSERT INTO [AspNetUsers] ([Id], [AccessFailedCount], [AllowEmailNotifications], [AllowMobileNotifications], [ConcurrencyStamp], [CreatedOn], [Email], [EmailConfirmed], [FirstName], [LastName], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UpdatedOn], [UserName])
    VALUES (N'1', 0, 1, 1, N'1d03f933-fdc4-4251-82c6-03115976214d', '2020-01-11T12:51:50.5130569-05:00', N'Wilsen@gmail.com', 0, N'Wilsen Humberto', N'Duarte cibo', 0, NULL, NULL, NULL, NULL, N'1231231', 0, NULL, 0, '2020-01-11T12:51:50.5130574-05:00', NULL),
    (N'2', 0, 1, 1, N'ca1d5158-ba2d-4355-8c6e-a45dcecaad3a', '2020-01-11T12:51:50.5136521-05:00', N'Breidy@gmail.com', 0, N'Breidy Alexis', N'Acevedo Diaz', 0, NULL, NULL, NULL, NULL, N'1231231', 0, NULL, 0, '2020-01-11T12:51:50.5136527-05:00', NULL),
    (N'3', 0, 1, 1, N'6d22483f-cd38-4007-a734-71477ee66195', '2020-01-11T12:51:50.5136733-05:00', N'Arnol@gmail.com', 0, N'Arnol Sebastian', N'Moreno Serrano', 0, NULL, NULL, NULL, NULL, N'1231231', 0, NULL, 0, '2020-01-11T12:51:50.5136735-05:00', NULL);
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'AllowEmailNotifications', N'AllowMobileNotifications', N'ConcurrencyStamp', N'CreatedOn', N'Email', N'EmailConfirmed', N'FirstName', N'LastName', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UpdatedOn', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
        SET IDENTITY_INSERT [AspNetUsers] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Name', N'UpdatedOn') AND [object_id] = OBJECT_ID(N'[StatusResource]'))
        SET IDENTITY_INSERT [StatusResource] ON;
    INSERT INTO [StatusResource] ([Id], [CreatedOn], [Name], [UpdatedOn])
    VALUES (1, '2020-01-11T12:51:50.5120483-05:00', N'Solicitado', '2020-01-11T12:51:50.5120489-05:00'),
    (2, '2020-01-11T12:51:50.5121130-05:00', N'Descartado', '2020-01-11T12:51:50.5121136-05:00'),
    (3, '2020-01-11T12:51:50.5121144-05:00', N'Aprobado', '2020-01-11T12:51:50.5121145-05:00');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Name', N'UpdatedOn') AND [object_id] = OBJECT_ID(N'[StatusResource]'))
        SET IDENTITY_INSERT [StatusResource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Name', N'UpdatedOn') AND [object_id] = OBJECT_ID(N'[TypeResource]'))
        SET IDENTITY_INSERT [TypeResource] ON;
    INSERT INTO [TypeResource] ([Id], [CreatedOn], [Name], [UpdatedOn])
    VALUES (1, '2020-01-11T12:51:50.5096925-05:00', N'Palabras', '2020-01-11T12:51:50.5113579-05:00'),
    (2, '2020-01-11T12:51:50.5115676-05:00', N'Refranes', '2020-01-11T12:51:50.5115688-05:00'),
    (3, '2020-01-11T12:51:50.5115701-05:00', N'Coplas', '2020-01-11T12:51:50.5115702-05:00'),
    (4, '2020-01-11T12:51:50.5115704-05:00', N'Leyendas', '2020-01-11T12:51:50.5115705-05:00');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Name', N'UpdatedOn') AND [object_id] = OBJECT_ID(N'[TypeResource]'))
        SET IDENTITY_INSERT [TypeResource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Audio', N'Content', N'CreatedOn', N'Disable', N'Name', N'Read', N'StatusResourceId', N'TypeResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[Resource]'))
        SET IDENTITY_INSERT [Resource] ON;
    INSERT INTO [Resource] ([Id], [Audio], [Content], [CreatedOn], [Disable], [Name], [Read], [StatusResourceId], [TypeResourceId], [UpdatedOn], [UserApplicationId])
    VALUES (CAST(1 AS bigint), NULL, N'¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).', '2020-01-11T12:51:50.5124373-05:00', 0, N'Palabra 1', 0, 1, 1, '2020-01-11T12:51:50.5125015-05:00', N'1'),
    (CAST(2 AS bigint), NULL, N'Pa qué se limpia las patas quien va a dormir en el suelo', '2020-01-11T12:51:50.5128727-05:00', 0, N'Palabra 2', 0, 1, 1, '2020-01-11T12:51:50.5128735-05:00', N'1'),
    (CAST(3 AS bigint), NULL, N'Loro con ala cortada es el que más aletea', '2020-01-11T12:51:50.5128766-05:00', 0, N'Palabra 3', 0, 1, 1, '2020-01-11T12:51:50.5128767-05:00', N'1'),
    (CAST(4 AS bigint), NULL, N'Borracho con real no estorba así arroje en la cantina', '2020-01-11T12:51:50.5128770-05:00', 0, NULL, 0, 2, 2, '2020-01-11T12:51:50.5128771-05:00', N'2'),
    (CAST(5 AS bigint), NULL, N'Más guapo que el caballo de Bolívar (En el llano guapo es sinónimo de valentía y coraje) Le hecho palo a todo mogote.', '2020-01-11T12:51:50.5128774-05:00', 0, NULL, 0, 2, 2, '2020-01-11T12:51:50.5128774-05:00', N'2'),
    (CAST(6 AS bigint), NULL, N'¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).', '2020-01-11T12:51:50.5128776-05:00', 0, NULL, 0, 2, 2, '2020-01-11T12:51:50.5128777-05:00', N'2'),
    (CAST(7 AS bigint), NULL, N'Nunca se cae de la cama, el que en el suelo se acuesta', '2020-01-11T12:51:50.5128779-05:00', 0, N'Copla 1', 0, 3, 3, '2020-01-11T12:51:50.5128780-05:00', N'3'),
    (CAST(8 AS bigint), NULL, N'¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).', '2020-01-11T12:51:50.5128782-05:00', 0, N'Copla 2', 0, 3, 3, '2020-01-11T12:51:50.5128783-05:00', N'3'),
    (CAST(9 AS bigint), NULL, N'Quien va a ser amansador sin montar potro cerrero', '2020-01-11T12:51:50.5128785-05:00', 0, N'Copla 3', 0, 3, 3, '2020-01-11T12:51:50.5128785-05:00', N'3'),
    (CAST(10 AS bigint), NULL, N'Me dejaste como como rolo de gender leña', '2020-01-11T12:51:50.5128787-05:00', 0, N'Copla 4', 0, 3, 3, '2020-01-11T12:51:50.5128788-05:00', N'3');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Audio', N'Content', N'CreatedOn', N'Disable', N'Name', N'Read', N'StatusResourceId', N'TypeResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[Resource]'))
        SET IDENTITY_INSERT [Resource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] ON;
    INSERT INTO [HistoryResource] ([Id], [CreatedOn], [Message], [ResourceId], [StatusResourceId], [UpdatedOn], [UserApplicationId])
    VALUES (CAST(1 AS bigint), '2020-01-11T12:51:50.5137960-05:00', NULL, CAST(1 AS bigint), 1, '2020-01-11T12:51:50.5137965-05:00', N'1');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] ON;
    INSERT INTO [HistoryResource] ([Id], [CreatedOn], [Message], [ResourceId], [StatusResourceId], [UpdatedOn], [UserApplicationId])
    VALUES (CAST(2 AS bigint), '2020-01-11T12:51:50.5139466-05:00', NULL, CAST(2 AS bigint), 2, '2020-01-11T12:51:50.5139472-05:00', N'2');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] ON;
    INSERT INTO [HistoryResource] ([Id], [CreatedOn], [Message], [ResourceId], [StatusResourceId], [UpdatedOn], [UserApplicationId])
    VALUES (CAST(3 AS bigint), '2020-01-11T12:51:50.5139501-05:00', NULL, CAST(3 AS bigint), 3, '2020-01-11T12:51:50.5139503-05:00', N'3');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'CreatedOn', N'Message', N'ResourceId', N'StatusResourceId', N'UpdatedOn', N'UserApplicationId') AND [object_id] = OBJECT_ID(N'[HistoryResource]'))
        SET IDENTITY_INSERT [HistoryResource] OFF;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_HistoryResource_ResourceId] ON [HistoryResource] ([ResourceId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_HistoryResource_StatusResourceId] ON [HistoryResource] ([StatusResourceId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_HistoryResource_UserApplicationId] ON [HistoryResource] ([UserApplicationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_Resource_StatusResourceId] ON [Resource] ([StatusResourceId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_Resource_TypeResourceId] ON [Resource] ([TypeResourceId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    CREATE INDEX [IX_Resource_UserApplicationId] ON [Resource] ([UserApplicationId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200111175151_initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200111175151_initial', N'2.2.6-servicing-10079');
END;

GO


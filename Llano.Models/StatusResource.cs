﻿using Llano.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class StatusResource:EntityBase<int>
    {
        public string Name { get; set; }

        public List<Resource> Resources { get; set; }
        public List<HistoryResource> HistoryResources { get; set; }
    }
}

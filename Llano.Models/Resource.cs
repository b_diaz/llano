﻿using Llano.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class Resource: EntityBase<long>
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string Audio { get; set; }
        public bool Read { get; set; }
        public bool Disable { get; set; } = false;


        public int TypeResourceId { get; set; } 
        public TypeResource TypeResource { get; set; }
        public int StatusResourceId { get; set; } = 1;
        public StatusResource StatusResource { get; set; }        
        public string UserApplicationId { get; set; }
        public Customer UserApplication { get; set; }
        public List<HistoryResource> HistoryResources { get; set; }
    }
}

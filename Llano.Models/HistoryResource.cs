﻿using Llano.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class HistoryResource: EntityBase<long>
    {
        public string Message { get; set; }
        public int StatusResourceId { get; set; }
        public StatusResource StatusResource { get; set; }
        public long ResourceId { get; set; }
        public Resource Resource { get; set; }
        public string UserApplicationId { get; set; }
        public Customer UserApplication { get; set; }

    }
}

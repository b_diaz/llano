﻿using Llano.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class TypeResource:EntityBase<int>
    {
        public string Name { get; set; }

        public List<Resource>  Resources { get; set; }
    }
}

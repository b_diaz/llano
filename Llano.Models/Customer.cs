﻿using Llano.Models.Base;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class Customer : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool AllowMobileNotifications { get; set; }
        public bool AllowEmailNotifications { get; set; }
        public bool Disabled { get; set; } = false;

        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;

        public List<Resource> Resources { get; set; }
        public List<HistoryResource> HistoryResources { get; set; }
    }

}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Models
{
    public class Notification
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public long ResourceId { get; set; }
        public int TypeId { get; set; }
        public string Type { get; set; }
        public bool? Tap { get; set; }
        public string Sound { get; set; } = "default";
        public string CustomerId { get; set; }
    }
}

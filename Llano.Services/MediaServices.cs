﻿using Llano.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Services
{
    public class MediaServices : IMediaServices
    {

        public async Task<string> SaveFile(Stream fileStreamIn, string filePathIn, string fileNameIn)
        {
            try
            {
                var extension = Path.GetExtension(fileNameIn).ToLower();

                string path;

                path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Audios", filePathIn);


                // Make sure the directory exists
                Directory.CreateDirectory(path);

                //var filePath = Path.Combine(path, fileName);
                var filePath = Path.Combine(path, Path.ChangeExtension(Path.GetRandomFileName(), Path.GetExtension(fileNameIn)));

                // Ensure file doesn't exist
                while (File.Exists(filePath))
                {
                    filePath = Path.Combine(path, Path.ChangeExtension(Path.GetRandomFileName(), Path.GetExtension(filePath)));
                }

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    // Write It
                    await fileStreamIn.CopyToAsync(stream);

                    string pdfUrl;
                    // Calculate the URL
                    pdfUrl = $"/Audios/{filePathIn}/{Path.GetFileName(filePath)}";
                    return (pdfUrl);
                }

            }
            catch (Exception)
            {

                throw new Exception("Tipo de archivo no valido");
            }

        }

        public bool Delete(string pathFile)
        {
            try
            {
                var listPathFile = pathFile.Split(':');

                foreach (var pathF in listPathFile)
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot" + pathF);

                    File.Delete(path);

                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}



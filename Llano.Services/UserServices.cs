﻿using Llano.DataAccess;
using Llano.Interfaces;
using Llano.Models;
using Llano.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Services
{
    public class UserServices: IUserServices
    {
        #region --------- Dependencias ------------
        private readonly UserManager<Customer> _userManager;
        private readonly IRepository<Resource, long > _ResourceRepository;

        public UserServices
            (
            UserManager<Customer> userManager,
            IRepository<Resource, long> ResourceRepository
            )
        {
            this._userManager = userManager;
            this._ResourceRepository = ResourceRepository;
        }
        #endregion

        #region ---------- POST ----------
        public async Task<bool> New(Customer cutomer, string password)
        {
           var IdentityResult = await _userManager.CreateAsync(cutomer, password);

            if (!IdentityResult.Succeeded)
            {
                return false;
            }
            return true;

        }
        #endregion

        #region ------- GET -----------
        public async Task<UserViewModel> GetUser(string idUser)
        {
            // solicitado = 1
            // descartado = 2
            // aprobado = 3

            var userApplication = await this._userManager.FindByIdAsync(idUser);

            var requested = await this._ResourceRepository.Query()
                .Where(x => x.UserApplicationId == idUser && x.StatusResourceId == 1)
                .ToListAsync();

            var approved = await this._ResourceRepository.Query()
                .Where(x => x.UserApplicationId == idUser && x.StatusResourceId == 3)
                .ToListAsync();

            var discarded = await this._ResourceRepository.Query()
                .Where(x => x.UserApplicationId == idUser && x.StatusResourceId == 2)
                .ToListAsync();

            var userViewModel = new UserViewModel()
            {
                UserApplication = userApplication,
                Approved = approved.Count,
                Discarded = discarded.Count,
                Requested = requested.Count
            };

            return userViewModel;
        }

        public async Task<Customer> GetUserByEmail(string email)
        {
            var customer = await this._userManager.FindByEmailAsync(email);   
            return customer;
        }


        #endregion

    }

}

﻿using Llano.DataAccess;
using Llano.Interfaces;
using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Services
{
    public class HistoryResourceServices: IHistoryResource
    {
        #region -------------- Dependencias ---------------

        private readonly IRepository<HistoryResource, long> _historyResourceRepository;

        public HistoryResourceServices
            (
            IRepository<HistoryResource, long> historyResourceRepository
            )
        {
            this._historyResourceRepository = historyResourceRepository;
        }

        #endregion

        #region ---------------- GET ----------------

        #endregion

        #region -------------- POST --------------------
        public void CreateHistoryResource(HistoryResource historyResource)
        {
            try
            {
                this._historyResourceRepository.Add(historyResource);
                this._historyResourceRepository.SaveChange();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion              
    }
}

﻿using Llano.Interfaces;
using Llano.Models;
using Llano.Services.Converters;
using Llano.ViewModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Services
{
    public class FcmService: IFcmService
    {
        public FcmService(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public async Task<string> SendNotification(Notification notification)
        {
            try
            {
                var message = new MessageViewModel
                {
                    Notification = new Notification
                    {
                        Title = notification.Title,
                        Body = notification.Body
                    },
                    Data = new CustomerData
                    {
                        Title = notification.Title,
                        Body = notification.Body,
                        TypeId = notification.TypeId,
                        Type = notification.Type,
                        ResourceId = notification.ResourceId

                    },
                    To = $"/topics/{notification.CustomerId}"
                };
                string resultString = await Send(message);
                return resultString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> Send(MessageViewModel message)
        {
            string resultString = "";
            using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, this.Configuration["NotificationsSettings:ApiUrl"]))
            {
                request.Headers.TryAddWithoutValidation("Authorization", this.Configuration["NotificationsSettings:AuthorizationKey"]);
                request.Headers.TryAddWithoutValidation("Content-Type", "application/json");
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new LowerCaseContractResolver()
                };
                string jsonString = JsonConvert.SerializeObject(message, Formatting.Indented, settings);
                var content = new StringContent(jsonString);

                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                request.Content = content;

                using (var httpClient = new HttpClient())
                {
                    var result = await httpClient.SendAsync(request);

                    if (result.IsSuccessStatusCode)
                    {
                        resultString = await result.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        resultString = await result.Content.ReadAsStringAsync();
                        throw new Exception($"Status Code:{result.StatusCode}, Error: {resultString}");
                    }
                }
            }
            return resultString;
        }

    }

}

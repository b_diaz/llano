﻿using Llano.DataAccess;
using Llano.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Llano.Interfaces;
using System.Collections.Generic;

namespace Llano.Services
{
    public class ResourceServices : IResourceService
    {

        #region --------- Dependencias ------------
        private readonly IRepository<Resource, long> _ResourceRepository;
        private readonly IRepository<HistoryResource, long> _historyResourceRepository;

        public ResourceServices
            (
            IRepository<Resource, long> ResourceRepository,
            IRepository<HistoryResource, long> historyResourceRepository
            )
        {
            this._ResourceRepository = ResourceRepository;
            this._historyResourceRepository = historyResourceRepository;
        }
        #endregion

        #region --------------- GET -------------------
        public async Task<List<Resource>> GetAll()
        {
            var Resources = await this._ResourceRepository.Query()
                .Where(x => x.Disable == false)
                .ToListAsync();

            return Resources;
        }

        public async Task<List<Resource>> GetForType(int TypeResourceId)
        {
            // aprobado = 3
            var Resources = await this._ResourceRepository.Query()
                .Where(x => x.TypeResourceId == TypeResourceId && x.Disable == false && x.StatusResourceId == 3)
                .Include(x => x.UserApplication)
                .Include(x => x.TypeResource)
                .ToListAsync();
            return Resources;
        }

        public async Task<List<Resource>> GetForTypeAndContain(int TypeResourceId, string Contain)
        {
            var Resources = await this._ResourceRepository.Query()
               .Where(x => x.TypeResourceId == TypeResourceId && (x.Content.Contains(Contain) || x.Name.Contains(Contain)) && x.Disable == false)
               .ToListAsync();

            return Resources;
        }

        public async Task<List<Resource>> GetForTypeAndStatus(int TypeResourceId, int StatusResourceId)
        {
            var Resources = await this._ResourceRepository.Query()
              .Where(x => x.TypeResourceId == TypeResourceId && x.StatusResourceId == StatusResourceId && x.Disable == false)
              .ToListAsync();

            return Resources;
        }

        public async Task<Resource> GetForId(long ResourceId)
        {
            var Resource = await this._ResourceRepository.Query()
                .Where(x => x.Id == ResourceId)
                .Include(x => x.UserApplication)
                .Include(x => x.TypeResource)
                .FirstOrDefaultAsync();

            return Resource;
        }

        public async Task<List<Resource>> GetForUserId(string UserApplicationId)
        {
            try
            {
                var Resources = await this._ResourceRepository.Query()
               .Where(x => x.UserApplication.Id == UserApplicationId)
               .Include(x => x.TypeResource)
               .Include(x => x.UserApplication)
               .ToListAsync();

                var r = Resources;


                return Resources;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        public async Task<List<Resource>> GetForUserAndTypeAndStatus(string UserApplicationId, int TypeResourceId, int StatusResourceId)
        {
            var Resources = await this._ResourceRepository.Query()
              .Where(x => x.TypeResourceId == TypeResourceId && x.StatusResourceId == StatusResourceId && x.Disable == false && x.UserApplicationId == UserApplicationId)
              .ToListAsync();

            return Resources;
        }


        public async Task<List<Resource>> GetForSatus(int StatusResourceId)
        {
            var Resources = await this._ResourceRepository.Query()
             .Where(x => x.StatusResourceId == StatusResourceId && x.Disable == false)
             .Include(x => x.UserApplication)
             .Include(x => x.TypeResource)
             .ToListAsync();

            return Resources;
        }

        public async Task<List<HistoryResource>> HistoryResource(long ResourceId)
        {
            /*
            List<HistoryResource> historiesResource = await this._ResourceRepository.Query()
                .Where(x => x.Id == ResourceId)
                .Include(x => x.HistoryResources).ThenInclude(x => x.StatusResource)
                .Include(x => x.HistoryResources).ThenInclude(x => x.UserApplication)
                .SelectMany(x => x.HistoryResources).ToListAsync();
             */

            List<HistoryResource> historiesResource = await this._historyResourceRepository.Query()
                .Where(x => x.ResourceId == ResourceId)
                .Include(x => x.StatusResource).ToListAsync();

            return historiesResource;
        }


        #endregion

        #region ----------------- POST -----------------
        public void CreateResource(Resource resource)
        {
            try
            {
                this._ResourceRepository.Add(resource);
                this._ResourceRepository.SaveChange();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region -------------- PUT -------------
        public async Task Update(long idResource, Resource resource)
        {
            try
            {
                var oldResource = await this._ResourceRepository.Query()
                    .Where(x => x.Id == idResource).FirstOrDefaultAsync();

                if (resource.Content != null && resource.Content.Length > 1 )
                {
                    oldResource.Content = resource.Content;
                }

                oldResource.Disable = resource.Disable;
                
                oldResource.StatusResourceId = resource.StatusResourceId;

                this._ResourceRepository.SaveChange();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task MarkAsRead(long idResource)
        {
            try
            {
                var resource = this._ResourceRepository.Query()
                .Where(x => x.Id == idResource)
                .FirstOrDefault();

                resource.Read = true;
                await this._ResourceRepository.SaveChangeAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<Resource>> ResourcesWithoutRead()
        {
            // solicitado = 1
            List<Resource> notifications = await this._ResourceRepository.Query()
                .Where(x => x.Read == false && x.Disable == false && x.StatusResourceId == 1)
                .Include(x => x.UserApplication)
                .Include(x => x.TypeResource)
                .ToListAsync();

            return notifications;
        }


        #endregion

        #region ------------ DELETE -----------
        public async Task Delete (long idResource)
        {
            try
            {
                var resource = await this._ResourceRepository.Query().Where( x => x.Id == idResource ).FirstOrDefaultAsync();
                this._ResourceRepository.Remove(resource);
                this._ResourceRepository.SaveChange();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}

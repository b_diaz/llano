using Llano.Interfaces;
using Llano.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        #region ---------- Inyeccion de dependencias ----------
        private readonly UserManager<Customer> _userManager;
        private IConfiguration _configuration;
        private IPathProvider _path;
        public EmailSender(IConfiguration configuration,
           IPathProvider pathProvider,
           UserManager<Customer> userManager
           )
        {
            _configuration = configuration;
            _userManager = userManager;
            _path = pathProvider;
        }
        #endregion

        public bool SendEmailResetPassword(string destinationEmail, string actionUrl, string nameUser)
        {
            try
            {
                //Informacion en el appsettings.json sobre las platillas de correo electronico
                var emailConfig = _configuration.GetSection("EmailConfig");
                var templateConfig = _configuration.GetSection("TemplatesConfig");
                var emailTemplate = templateConfig.GetSection("SendResetPassword").Value;
                string emailTemplatePath = _path.MapPath($"/EmailFormats/{emailTemplate}");

                //Informacion en el appsettings.json sobre con configuracion SMTP
                var smtpHost = emailConfig.GetSection("SmtpHost").Value;
                var smtpUser = emailConfig.GetSection("SmtpUser").Value;
                var smtpPass = emailConfig.GetSection("SmtpPass").Value;

                SmtpClient smtpClient = new SmtpClient(smtpHost)
                {
                    Credentials = new System.Net.NetworkCredential(smtpUser, smtpPass)
                };

                MailMessage mail = new MailMessage(smtpUser, destinationEmail);

                mail.Subject = "Restablecer contrase�a LlanoApp! ";
                mail.IsBodyHtml = true;

                StringBuilder body = new StringBuilder(File.ReadAllText(emailTemplatePath));
                body.Replace("{{PREHEADER}}", "Mensaje de la plataforma LlanoApp");
                body.Replace("{{NOMBRE}}", nameUser);
                body.Replace("{{MENSAJE}}", "�Parece que olvidaste la contrase�a!, No pasa nada nosotros te ayudamos a recuperar el acceso <br/><br/> Recuerda que la union hace la fuerza");
                body.Replace("{{ACTIONURL}}", actionUrl);
                body.Replace("{{ACTIONNAME}}", "Restablecer Contrase�a");
                body.Replace("{{NOMBREDELAEMPRESA}}", "Iniciativa LlanoApp");

                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.Body = body.ToString();

                smtpClient.Send(mail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region ---------- EmailConfirmation ----------
        public bool SendEmailWelcome(string destinationEmail, string actionUrl, string nameUser)
        {
            try
            {
                //Informacion en el appsettings.json sobre las platillas de correo electronico
                var emailConfig = _configuration.GetSection("EmailConfig");
                var templateConfig = _configuration.GetSection("TemplatesConfig");
                var emailTemplate = templateConfig.GetSection("SendWelcome").Value;
                string emailTemplatePath = _path.MapPath($"/EmailFormats/{emailTemplate}");

                //Informacion en el appsettings.json sobre con configuracion SMTP
                var smtpHost = emailConfig.GetSection("SmtpHost").Value;
                var smtpUser = emailConfig.GetSection("SmtpUser").Value;
                var smtpPass = emailConfig.GetSection("SmtpPass").Value;

                SmtpClient smtpClient = new SmtpClient(smtpHost)
                {
                    Credentials = new System.Net.NetworkCredential(smtpUser, smtpPass)
                };

            MailMessage mail = new MailMessage(smtpUser, destinationEmail);

                mail.Subject = "Bienvenido a LlanoApp! ";
                mail.IsBodyHtml = true;

                StringBuilder body = new StringBuilder(File.ReadAllText(emailTemplatePath));
                body.Replace("{{PREHEADER}}", "Mensaje de la plataforma LlanoApp");
                body.Replace("{{NOMBRE}}", nameUser);
                body.Replace("{{MENSAJE}}", "�Bienvenido a LlanoApp, Desde ahora eres parte de la familia de llaneros que con peque�os aportes de contenido (palabras, coplas , refranes y leyendas) ayudan a preservar la cultura Llanera.<br/><br/> Conoce un poco mas sobre esta iniciativa.");
                body.Replace("{{ACTIONURL}}", actionUrl);
                body.Replace("{{ACTIONNAME}}", "Conocer M�s");
                body.Replace("{{NOMBREDELAEMPRESA}}", "Iniciativa LlanoApp");

                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.Body = body.ToString();

                smtpClient.Send(mail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}

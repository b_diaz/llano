﻿using Llano.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Services
{
    /// <summary>
    /// Permite trabajar con las rutas del servidor
    /// </summary>
    public class PathProvider : IPathProvider
    {
        /// <summary>
        /// Información del Hosting de la aplicación
        /// </summary>
        private IHostingEnvironment _hostingEnvironment;

        /// <summary>
        /// Ruta Raiz del Servidor
        /// </summary>
        public string WebRootPath => _hostingEnvironment.WebRootPath;

        /// <summary>
        /// Construye un objeto para el trabajo con las rutas
        /// </summary>
        /// <param name="environment">Info del hosting de la App</param>
        public PathProvider(IHostingEnvironment environment)
        {
            _hostingEnvironment = environment;
        }

        /// <summary>
        /// Mapea rutas relativas a rutas absolutas
        /// </summary>
        /// <param name="path">Ruta relativa</param>
        /// <returns>Ruta absoluta obtenida a partir de la ruta relativa dada</returns>
        public string MapPath(string path)
        {
            var filePath = (_hostingEnvironment.WebRootPath + path);
            return filePath;
        }
    }
}

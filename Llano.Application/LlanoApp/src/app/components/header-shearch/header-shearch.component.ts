import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { ResourceSearchPage } from 'src/app/pages/Resources/resource-search/resource-search.page';
import { Resource } from 'src/app/models/Resource';

@Component({
  selector: 'app-header-shearch',
  templateUrl: './header-shearch.component.html',
  styleUrls: ['./header-shearch.component.scss'],
})
export class HeaderShearchComponent implements OnInit {

  @Input() title: string;
  @Input() resources:Resource[];
  @Input() resourceTypeId:number[];  
  
  constructor(private location: Location, public modalController: ModalController) { }

  ngOnInit() {}

  back() {
    this.location.back();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ResourceSearchPage,
      componentProps: {
        'resources': this.resources,
        'resourceTypeId': this.resourceTypeId
      }
    });
    return await modal.present();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderBackComponent } from './header-back/header-back.component';
import { IonicModule } from '@ionic/angular';
import { HeaderShearchComponent } from './header-shearch/header-shearch.component';
import { PlayerComponent } from './player/player.component';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { FlashCardComponent } from './flash-card/flash-card';


@NgModule({
  declarations: [
    HeaderBackComponent,
     HeaderShearchComponent,
      PlayerComponent,
      FlashCardComponent
    ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  exports: [
    HeaderBackComponent,
     HeaderShearchComponent,
      PlayerComponent,
      FlashCardComponent
    ],
  providers: [
    DatePipe
  ]
})
export class ComponentsModule { }

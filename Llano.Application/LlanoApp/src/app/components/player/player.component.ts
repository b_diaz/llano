import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { Platform, LoadingController, ToastController } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { File } from '@ionic-native/file/ngx';
import { DatePipe } from '@angular/common';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit, OnChanges, OnDestroy {

  @Input() audioPath: string;
  @Input() urlAudio: string;

  filename: string;
  currPlayingFile: MediaObject;
  storageDirectory: any;

  isPlaying: boolean;
  isInPlay: boolean;
  isReady: boolean;

  message: any;

  duration: any = -1;
  position: any = 0;

  getDurationInterval: any;
  getPositionInterval: any;

  constructor(
    private platform: Platform,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private file: File,
    private transfer: FileTransfer,
    private media: Media,
    private datePipe: DatePipe,
    private route: Router
  ) {
    console.log("se construye componente");

    this.isPlaying = false;
    this.isInPlay = false;
    this.isReady = false;

    this.storageDirectory = this.file.externalDataDirectory;

  }

  ngOnChanges(changes: SimpleChanges) {
    const url = this.route.url;
    if (!url.includes('resource-detail')) {
      console.log('Componente media player detecta cambios');
      if (!isNullOrUndefined(this.currPlayingFile)) {
          this.stopPlayRecording();
          this.currPlayingFile = null;
          this.file = null;
        //this.ngOnDestroy();
      } else {
        this.ngOnInit();
      }
    }
  }

  ngOnInit() {
    if (!isNullOrUndefined(this.audioPath) || !isNullOrUndefined(this.urlAudio)) {
      if (!isNullOrUndefined(this.audioPath)) {
        console.log('Imprimiendo audio que imgresa al componente', this.audioPath);
        this.prepareAudioFileInternal(this.audioPath);
      } else {
        this.filename = this.urlAudio.split('/').pop();
        this.prepareAudioFileExternal(this.urlAudio);
      }
    } 

  }

  prepareAudioFileExternal(urlFile: string) {
    this.platform.ready().then(() => {
      this.file
        .resolveDirectoryUrl(this.storageDirectory)
        .then(resolvedDirectory => {
          console.log('resolved  directory: ' + resolvedDirectory.nativeURL);
          this.file
            .checkFile(resolvedDirectory.nativeURL, this.filename)
            .then(data => {
              if (data === true) {
                // exist
                this.getDurationAndSetToPlay();
              } else {
                // not sure if File plugin will return false. go to download
                console.log('not found!');
                throw { code: 1, message: 'NOT_FOUND_ERR' };
              }
            })
            .catch(async err => {
              console.log('Error occurred while checking local files:');
              console.log(err);
              if (err.code === 1) {
                // not found! download!
                console.log('not found! download!');
                const loadingEl = await this.loadingCtrl.create({
                  message: 'Descargando el audio del recurso...'
                });
                loadingEl.present();
                const fileTransfer: FileTransferObject = this.transfer.create();
                fileTransfer
                  .download(urlFile, this.storageDirectory + this.filename)
                  .then(entry => {
                    console.log('download complete' + entry.toURL());
                    loadingEl.dismiss();
                    this.getDurationAndSetToPlay();
                  })
                  .catch(err2 => {
                    console.log('Download error!');
                    loadingEl.dismiss();
                    console.log(err2);
                  });
              }
            });
        });
    });
  }

  prepareAudioFileInternal(audio: any) {
    console.log('Ingresa en prepareAudioFileInternal ', audio);

    this.getDurationAndSetToPlay();
  }

  createAudioFileExternal(pathToDirectory, filename): MediaObject {
    return this.media.create(pathToDirectory + filename);
  }
  createAudioFileInternal(audioPath: string): MediaObject {
    return this.media.create(audioPath);
  }

  getDurationAndSetToPlay() {
    console.log('getDurationAndSetToPlay');


    if (!isNullOrUndefined(this.audioPath)) {

      this.currPlayingFile = this.createAudioFileInternal(this.audioPath);

    } else {
      this.currPlayingFile = this.createAudioFileExternal(
        this.storageDirectory,
        this.filename
      );

    }

    this.currPlayingFile.play();
    this.currPlayingFile.setVolume(0.0); // you don't want users to notice that you are playing the file
    let self = this;
    this.getDurationInterval = setInterval(function () {
      if (self.duration == -1) {
        self.duration = ~~self.currPlayingFile.getDuration(); // make it an integer
      } else {
        self.currPlayingFile.stop();
        self.currPlayingFile.release();
        self.setRecordingToPlay();
        clearInterval(self.getDurationInterval);
      }
    }, 100);
  }

  getAndSetCurrentAudioPosition() {
    console.log('getAndSetCurrentAudioPosition');

    let diff = 1;
    let self = this;
    this.getPositionInterval = setInterval(function () {
      let last_position = self.position;
      self.currPlayingFile.getCurrentPosition().then(position => {
        if (position >= 0 && position < self.duration) {
          if (Math.abs(last_position - position) >= diff) {
            // set position
            self.currPlayingFile.seekTo(last_position * 1000);
          } else {
            // update position for display
            self.position = position;
          }
        } else if (position >= self.duration) {
          self.stopPlayRecording();
          self.setRecordingToPlay();
        }
      });
    }, 100);
  }

  setRecordingToPlay() {
    console.log('setRecordingToPlay');
    if (!isNullOrUndefined(this.audioPath)) {
      this.currPlayingFile = this.createAudioFileInternal(this.audioPath);

    } else {
      this.currPlayingFile = this.createAudioFileExternal(
        this.storageDirectory,
        this.filename
      );

    }
    this.currPlayingFile.onStatusUpdate.subscribe(status => {
      // 2: playing
      // 3: pause
      // 4: stop
      this.message = status;
      switch (status) {
        case 1:
          this.isInPlay = false;
          break;
        case 2: // 2: playing
          this.isInPlay = true;
          this.isPlaying = true;
          break;
        case 3: // 3: pause
          this.isInPlay = true;
          this.isPlaying = false;
          break;
        case 4: // 4: stop
        default:
          this.isInPlay = false;
          this.isPlaying = false;
          break;
      }
    });
    console.log('audio file set');
    this.message = 'audio file set';
    this.isReady = true;
    this.getAndSetCurrentAudioPosition();
  }

  playRecording() {
    this.currPlayingFile.play();
    this.toastCtrl
      .create({
        message: `Reproduciendo desde ${this.fmtMSS(this.position)}`,
        duration: 2000
      })
      .then(toastEl => toastEl.present());
  }

  pausePlayRecording() {
    this.currPlayingFile.pause();
    this.toastCtrl
      .create({
        message: `Pausado en ${this.fmtMSS(this.position)}`,
        duration: 2000
      })
      .then(toastEl => toastEl.present());
  }

  stopPlayRecording() {
    this.currPlayingFile.stop();
    this.currPlayingFile.release();
    clearInterval(this.getPositionInterval);
    this.position = 0;
  }

  fmtMSS(s) {
    return this.datePipe.transform(s * 1000, 'mm:ss');

    /** The following has been replaced with Angular DatePipe */
    // // accepts seconds as Number or String. Returns m:ss
    // return (
    //   (s - // take value s and subtract (will try to convert String to Number)
    //     (s %= 60)) / // the new value of s, now holding the remainder of s divided by 60
    //     // (will also try to convert String to Number)
    //     60 + // and divide the resulting Number by 60
    //   // (can never result in a fractional value = no need for rounding)
    //   // to which we concatenate a String (converts the Number to String)
    //   // who's reference is chosen by the conditional operator:
    //   (9 < s // if    seconds is larger than 9
    //     ? ':' // then  we don't need to prepend a zero
    //     : ':0') + // else  we do need to prepend a zero
    //   s
    // ); // and we add Number s to the string (converting it to String as well)
  }

  ngOnDestroy() {
    console.log('Ondestroy del compornente reproductor');
    if (!isNullOrUndefined(this.currPlayingFile)) {
      this.stopPlayRecording();
    }
  }

}

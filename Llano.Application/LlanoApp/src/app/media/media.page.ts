import { Component, OnInit, OnDestroy } from '@angular/core';
import { ResourceService } from '../services/resource.service';
import { Resource } from '../models/Resource';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-media',
  templateUrl: './media.page.html',
  styleUrls: ['./media.page.scss'],
})
export class MediaPage implements OnInit, OnDestroy {

  error: boolean;
  resources: Resource[];
  resourcesSelect: Resource[];
  loading: HTMLIonLoadingElement;
  typeStatus: any[];
  typeStatusSelect: number;

  resource1Subscribe: Subscription;
  resource2Subscribe: Subscription;

  constructor(
    private resourceService: ResourceService,
    private loadingController: LoadingController,
    private storage: Storage
  ) {
    this.error = false;
    this.typeStatus = [
      { name: 'Solicitado', id: 1 },
      { name: 'Descartado', id: 2 },
      { name: 'Aprobado', id: 3 },
    ];
  }

  async ngOnInit() {
    this.typeStatusSelect = 1;
  }

  async ionViewDidEnter() {

    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: 'Por favor espere ..',
    });
    await this.loading.present();
    // this.ionViewWillEnter();

    this.storage.get('idUser').then(
      (resId: string) => {
        this.getResourcesThisUser(resId);
      },
      error => {
        console.error("Algo salio mal recuperando el idUser del Storage", error);
        this.resources = null;
        this.error = true;
        this.loading.dismiss();
      }
    )

  }

  getResourcesThisUser(idUser: string) {
    this.resource1Subscribe = this.resourceService.getResourcesThisUser(idUser).pipe(
    ).subscribe(
      (res: Resource[]) => {
        console.log('Estos son los recursos creados por el usuario', res);
        this.resourceService.sendResources(res.reverse());
        this.loadResources();
      }, error => {
        console.error(error);
        this.resources = null;
        this.error = true;
        this.loading.dismiss();
      }
    );
  }

  loadResources() {
    this.resource2Subscribe =  this.resourceService.getResources().subscribe(
      res => {
        this.error = false;
        this.resources = res;
        if (this.resources.length >= 0) {
          this.resourcesSelect = this.resources.filter(x => { return x.statusResourceId == this.typeStatusSelect });
        }
        this.loading.dismiss();
      }, error => {
        console.error("Algo salio mal", error);
        this.resources = null;
        this.error = true;
        this.loading.dismiss();
      }
    )
  }

  segmentChangedStatus(ev: any) {
    this.typeStatusSelect = ev.detail.value;
    console.log('Cambia de status', this.typeStatusSelect);
    console.log('Los recursos al ingresar son: ', this.resources);

    this.resourcesSelect = this.resources.filter(x => { return x.statusResourceId == this.typeStatusSelect });
    console.log('Retorna estos recursos', this.resourcesSelect);
  }

  doRefresh(ev: any) {
    this.loadResources();
    setTimeout(() => {
      console.log('Refrescando');
      ev.target.complete();
    }, 3000);
  }


  ionViewWillLeave() {
    if (this.resource1Subscribe) {
      this.resource1Subscribe.unsubscribe();
    }

    if (this.resource2Subscribe) {
      this.resource2Subscribe.unsubscribe();
    }

    if (this.loading) {
      this.loading.dismiss();
    }
  }


  ngOnDestroy() {

  }


}

import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, throwError, Observable, of } from 'rxjs';
import { catchError, filter, take, switchMap, finalize, mergeMap, } from 'rxjs/operators';
import { HttpRequest, HttpErrorResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AuthService } from '../services/auth.service';
import { from } from 'rxjs';
import { AlertController, NavController } from '@ionic/angular';
import { AuthData } from '../models/AuthData';


@Injectable({
  providedIn: 'root'
})
export class HttpInteceptor implements HttpInterceptor {


  token: string;
  refreshTokenInProgress = false;
  refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private _storage: Storage,
    private _authService: AuthService,
    private _alert: AlertController,
    private _nav: NavController
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //console.info("Ingresa eme HttpInterceptors")
    // if (!req.headers.has('Content-Type')) {
    //   req = req.clone({
    //     headers: req.headers.set('Content-Type', 'application/json')
    //   });
    // }


    let promise = this._storage.get("AuthData")
    var observableFromPromise = from(promise);
    return observableFromPromise.pipe(
      mergeMap((token: AuthData) => {

        console.log("Respuesta de merge", token);
        
        //Si existe el token agreguelo al header de la solicitud
        if (token) {
          this.token = token.access_token;
          req = this.addAuthenticationToken(req, token.access_token);
        }
        return next.handle(req).pipe(
          catchError((error: HttpErrorResponse) => {
            console.log("captura error", error);
            if (!req.url.match(/Resource\//)) {
              return next.handle(req);
            }
            if (error && error.status === 401) {
              console.log("Entra en 401")
              // 401 errors are most likely going to be because we have an expired token that we need to refresh.
              if (this.refreshTokenInProgress) {
                // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
                // which means the new token is ready and we can retry the request again
                return this.refreshTokenSubject.pipe(
                  filter(result => result !== null),
                  take(1),
                  switchMap(() => next.handle(this.addAuthenticationToken(req, token.access_token)))
                );
              } else {
                this.refreshTokenInProgress = true;

                // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
                this.refreshTokenSubject.next(null);

                return this._authService.getRefreshToken().pipe(
                  catchError(error => {
                    console.error(error);
                    this.presentAlert();
                    return throwError("El Usario debe Iniciar sesion Nuevamente", error);
                  }),
                  switchMap((success: any) => {
                    this.token = success.access_token;
                    this._storage.set('AuthData', success)
                    this.refreshTokenSubject.next(true);
                    return next.handle(this.addAuthenticationToken(req, success.access_token));
                  }),
                  // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                  // for the next time the token needs to be refreshed
                  finalize(() => this.refreshTokenInProgress = false)
                );
              }
            } else {
              return throwError(error);
            }
          })
        );
      })
    )

  }

  private addAuthenticationToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    // If we do not have a token yet then we should not set the header.
    // Here we could first retrieve the token from where we store it.
    if (!this.token) {
      return request;
    }


    //https://api.appcenter.ms/
    // If you are calling an outside domain then do not add the token.
    //if (!request.url.match(/www.mydomain.com\//)) {
    // return request;
    //}
    return request.clone({
      headers: request.headers.set("Authorization", "Bearer " + token)
    });
  }

  async presentAlert() {
    const alert = await this._alert.create({          //header: 'Seccion Cerrada',

      header: 'Sesión cerrada',
      subHeader: 'La sesión a sido cerrada, inicia nuevamente',
      message: 'Este tipo de cierre puede darse por acceso desde otro dispositivo',
      buttons: [{
        text: 'Ok',
        cssClass: 'warning',
        handler: () => {
          this._nav.navigateRoot(['/welcome']);
        },

      }],
      backdropDismiss: false,
      cssClass: 'alertCustom'
    });

    await alert.present();
  }
}

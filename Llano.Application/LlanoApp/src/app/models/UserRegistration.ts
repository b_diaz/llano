export interface UserRegistration {


    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    password: string;
    allowMobileNotifications: boolean;
    allowEmailNotifications: boolean;

}

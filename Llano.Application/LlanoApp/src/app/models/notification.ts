export class Notification {
    title: string;
    body: string;
    resourceId: number;
    typeId: number;
    type: string;
    tap: boolean;
}

﻿import { Resource } from './Resource';
import { HistoryResource } from './HistoryResource';

export interface UserApplication {

    id: string;
    identityUserId: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    allowMobileNotifications: boolean;
    allowEmailNotifications: boolean;

    resources: Resource[];
    historyResources: HistoryResource[];

    createdOn: Date | string;
    updatedOn: Date | string;
}


import { UserApplication } from './UserApplication';

export interface UserViewModel {
    userApplication:  UserApplication;
    requested: number;
    approved: number;
    discarded: number;
} 
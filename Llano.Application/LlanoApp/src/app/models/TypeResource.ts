﻿import { Resource } from './Resource';

export interface TypeResource {
    id: number;
    name: string;

    resources: Resource[];

    createdOn: Date | string;
    updatedOn: Date | string;
}

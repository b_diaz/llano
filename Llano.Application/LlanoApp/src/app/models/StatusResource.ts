﻿import { Resource } from './Resource';
import { HistoryResource } from './HistoryResource';

export interface StatusResource {
    id: number;
    name: string;

    resources: Resource[];
    historyResources: HistoryResource[];

    createdOn: Date | string;
    updatedOn: Date | string;
}

﻿import { TypeResource } from './TypeResource';
import { StatusResource } from './StatusResource';
import { UserApplication } from './UserApplication';
import { HistoryResource } from './HistoryResource';

export interface Resource {
    id: number;
    name: string;
    content: string;
    audio: string;
    read: boolean;
    disable: boolean;

    typeResourceId: number;
    typeResource: TypeResource;
    statusResourceId: number;
    statusResource: StatusResource;
    userApplicationId: string;
    userApplication: UserApplication;
    historyResources: HistoryResource[];

    createdOn: Date | string;
    updatedOn: Date | string;
}
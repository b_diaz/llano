﻿import { StatusResource } from './StatusResource';
import { Resource } from './Resource';
import { UserApplication } from './UserApplication';

export interface HistoryResource {
    id: number;
    statusResourceId: number;
    statusResource: StatusResource;
    resourceId: number;
    resource: Resource;
    userApplicationId: string;
    userApplication: UserApplication;

    createdOn: Date | string;
    updatedOn: Date | string;
}

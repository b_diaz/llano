import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'resource-list/:typeId', loadChildren: './pages/Resources/resource-list/resource-list.module#ResourceListPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  {
    path: 'resource-detail/:resourceId/:typeId',
    loadChildren: './pages/Resources/resource-detail/resource-detail.module#ResourceDetailPageModule'
  },
  { path: 'resource-create/:typeId', loadChildren: './pages/Resources/resource-create/resource-create.module#ResourceCreatePageModule' },
  { path: 'media', loadChildren: './media/media.module#MediaPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'welcome', loadChildren: './pages/Auth/welcome/welcome.module#WelcomePageModule' },
  { path: 'login', loadChildren: './pages/Auth/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/Auth/register/register.module#RegisterPageModule' },
  { path: 'tour', loadChildren: './pages/Auth/tour/tour.module#TourPageModule' },
  { path: 'notifications-alert', loadChildren: './pages/Notifications/notifications-alert/notifications-alert.module#NotificationsAlertPageModule' }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

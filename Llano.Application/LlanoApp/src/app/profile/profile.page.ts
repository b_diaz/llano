import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, NavController } from '@ionic/angular';
import { UserApplication } from '../models/UserApplication';
import { UserService } from '../services/user.service';
import { UserViewModel } from '../models/UserViewModel';
import { Storage } from '@ionic/storage';
import { FcmService } from '../services/fcm.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  error: boolean;
  loading: HTMLIonLoadingElement;
  idUser: string;
  userView: UserViewModel;

  constructor(
    public modalCtrl: ModalController,
    private userService: UserService,
    public loadingController: LoadingController,
    private storage: Storage,
    private navController: NavController,
    private firebaseService: FcmService
  ) {
  }

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: 'Por favor espere ..',
    });
    await this.loading.present();

    this.storage.get('idUser').then(
      res => {
        console.log("Imprimiendo respuesta idUser", res);
        this.idUser = res;
       
        this.getInfo(this.idUser);
      },
      error => {
        console.error("Error obteniendo idUser del storage", error);
      }
    )

  }

  getInfo(iduser: string) {
    this.userService.getInfo(this.idUser).pipe(
    ).subscribe(
      (res: UserViewModel) => {
        this.error = false;
        this.userView = res;
        console.log(this.userView);
        this.loading.dismiss();
      }, error => {
        console.error(error);
        this.userView = null;
        this.error = true;
        this.loading.dismiss();
      }
    );
  }

  async logout() {
    this.storage.keys().then(
      async keys => {
        keys.forEach(key => {
          if (key != 'idUser') {
            this.storage.remove(key)
          }
        });

        this.storage.get('idUser').then(
          res => {
            this.closeFuncionsFirebase(res);
          }, error => {
            console.error("No se ha podido cerrar las funciones de firebase para este usuario", error);
          }
        )

        this.navController.navigateRoot(['/welcome']);
      }
    );
  }

  closeFuncionsFirebase(idUser: string) {
    this.firebaseService.unsubscribeTopicUser(idUser);
  }

}

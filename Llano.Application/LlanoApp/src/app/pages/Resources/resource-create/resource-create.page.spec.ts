import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceCreatePage } from './resource-create.page';

describe('ResourceCreatePage', () => {
  let component: ResourceCreatePage;
  let fixture: ComponentFixture<ResourceCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceCreatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalController, AlertController, LoadingController, ToastController, NavController } from '@ionic/angular';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { File } from '@ionic-native/file/ngx';
import { ResourceService } from 'src/app/services/resource.service';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Resource } from 'src/app/models/Resource';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-resource-create',
  templateUrl: './resource-create.page.html',
  styleUrls: ['./resource-create.page.scss'],
})
export class ResourceCreatePage implements OnInit {

  namePage: string;
  typeId: number;
  formResource: FormGroup;

  idUser: string;

  loadingAudio: HTMLIonLoadingElement;
  loadingSend: HTMLIonLoadingElement;

  audio: any;
  audioRequired: boolean;
  clickSend: boolean;

  audioPath: string;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public modalController: ModalController,
    private fileChooser: FileChooser,
    private file: File,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private resourceService: ResourceService,
    private navController: NavController,
    private filePath: FilePath,
    private zone: NgZone,
    private storage: Storage
  ) {
    this.formResource = formBuilder.group({
      name: ['', Validators.required],
      content: ['', Validators.required],

      typeResourceId: [],
      userApplicationId: []
    });

    this.audio = null;
    this.audioRequired = false;
    this.clickSend = false;
  }

  /*
  GENERADOS EN API
   id: number;
   typeResource: TypeResource;
   statusResourceId: number;
   statusResource: StatusResource;
   read: boolean;
   disable: boolean;
   userApplication: UserApplication;
   historyResources: HistoryResource[];

  ENTRADA DE USUARIO
  name: string;
  content: string;
  audio: string;
  RECUPERADOS DE LA APP

   typeResourceId: number;
   userApplicationId: string;

  */





  ngOnInit() {

    this.storage.get('idUser').then(
      (resId: string) => {
        this.idUser = resId;
        this.typeId = parseInt(this.route.snapshot.paramMap.get('typeId'), 10);

        switch (this.typeId) {
          case 1:
            this.namePage = 'Crear Palabra';
            break;
          case 2:
            this.namePage = 'Crear Refran';
            break;
          case 3:
            this.namePage = 'Crear Copla';
            break;
          case 4:
            this.namePage = 'Crear Leyenda';
            this.audioRequired = true;
            break;

          default:
            break;
        }

        this.formResource.controls.typeResourceId.setValue(String(this.typeId));
        this.formResource.controls.userApplicationId.setValue(this.idUser);
      }, error => {
        console.error("Error obteniendo idUser");
      }
    )


  }

  selectFile() {
    // tslint:disable-next-line: quotemark
    this.fileChooser.open({ "mime": "audio/*" })
      .then(uri => {
        this.filePath.resolveNativePath(uri)
          .then(filePath => {
            this.getFile(filePath);
          }, async error => {
            // tslint:disable-next-line: quotemark
            if (error === "column '_data' does not exist") {
              this.getFile(uri);
            } else {
              console.error('Error no controlado en resolveNativePath', error);
              await this.presentAlertFile();
            }
          });
      });
  }


  getFile(path: string) {
    this.file.resolveLocalFilesystemUrl(path)
      .then((response: any) => {
        console.log('Imprimiendo response', response);

        const nativeUrl: string = response.nativeURL;
        response.file(async (file: any) => {
          const type = file.type.split('/')[0];
          if (type !== 'audio') {
            await this.presentAlertFormato();
          } else {

            if (!nativeUrl.includes('file:///')) {
              const fileName: string = response.fullPath.split('root_path/').pop();
              this.audioPath = 'file:///' + fileName;
            } else {
              this.audioPath = path;
            }

            this.makeFileIntoBlob(response.nativeURL, file.type, response.name);
          }
        }, async error => {
          console.log('Error ', error);
          await this.presentAlertFile();
        });
      }, async error => {
        console.error('Imprimiendo error response ', error);
        await this.presentAlertFile();
      }
      );
  }

  async presentAlertFormato() {
    const alert = await this.alertController.create({
      header: 'Formato Invalido',
      subHeader: '¿Seguro que seleccionaste un audio?',
      message: 'Solo se permiten audios',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async presentAlertFile() {
    const alert = await this.alertController.create({
      header: 'No hemos podido recuperar el archivo',
      subHeader: 'No se puede acceder a la sdCard',
      message: 'Solo se permiten audios',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  async makeFileIntoBlob(audioPath: string, type: string, fileName: string) {
    this.loadingAudio = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
       message: 'Procesando Audio..' 
      });
    this.loadingAudio.present();
    return new Promise((resolve, reject) => {
      this.file.resolveLocalFilesystemUrl(audioPath).then((fileEntry: any) => {
        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var audioBlob: any = new Blob([evt.target.result], { type: type });
            console.log("Imprimiendo audioBlob ", audioBlob);

            audioBlob.name = fileName;
            this.zone.run(() => {
              this.audio = audioBlob;
            })


            this.loadingAudio.dismiss();
            resolve(audioBlob);
          };
          reader.onerror = async (e) => {
            let toast = await this.toastController.create({ message: 'Error al procesar imagen' });
            this.loadingAudio.dismiss();
            toast.present();
            reject(e);
          };

          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }

  async sendResource() {

    if (!this.formResource.valid || (!this.audio && this.audioRequired)) {
      this.validateAllFormFields(this.formResource);
      this.clickSend = true;
    } else {
      this.loadingSend = await this.loadingController.create(
        { 
          spinner: null,
          cssClass: 'custom-loader',
          message: 'Enviando aporte...' 
      });
      this.loadingSend.present();

      const resource = new FormData();
      resource.append('Name', this.formResource.controls['name'].value);
      resource.append('Content', this.formResource.controls['content'].value);
      resource.append('TypeResourceId', this.formResource.controls['typeResourceId'].value);
      resource.append('UserApplicationId', this.formResource.controls['userApplicationId'].value);
      if (this.audio) {
        resource.append('Audio', this.audio, this.audio.name);
      }

      this.resourceService.createResource(resource).subscribe(
        async (res: Resource[]) => {
          let resReverse = res.reverse();
          this.resourceService.sendResources(resReverse);
          const toast = await this.toastController.create({
            message: 'Se ha creado un nuevo recurso',
            duration: 3000
          });
          this.loadingSend.dismiss();
          toast.present();
          this.navController.navigateForward(['/tabs/media']);
        }, async error => {
          const toast = await this.toastController.create({
            message: 'Se ha producido un error al enviar',
            duration: 3000
          });
          this.loadingSend.dismiss();
          toast.present();
        });

    }

  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  deleteAudio() {
    this.audioPath = null;
    this.audio = null;
  }

  ionViewWillLeave() {
    this.audioPath = null;
    this.audio = null;
    if (this.loadingAudio) {
      this.loadingAudio.dismiss();
    }
    if (this.loadingSend) {
      this.loadingSend.dismiss();
    }
  }


}

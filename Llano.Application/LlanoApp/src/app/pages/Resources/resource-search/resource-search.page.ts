import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Resource } from 'src/app/models/Resource';
import { Location } from '@angular/common';
import { ModalController, NavController, IonSearchbar } from '@ionic/angular';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-resource-search',
  templateUrl: './resource-search.page.html',
  styleUrls: ['./resource-search.page.scss'],
})
export class ResourceSearchPage implements OnInit {

  @Input() resources: Resource[];
  @Input() resourceTypeId: number;

  @ViewChild('autofocus', { static: false }) searchbar: IonSearchbar;

  textShearch: string;
  resourcesMatch: Resource[];

  constructor(
    private location: Location,
    private modalController: ModalController,
    private navController: NavController
  ) { }

  ngOnInit() {
    setTimeout(() => this.searchbar.setFocus(), 300);
    console.log('entra e imprime', this.resources);

  }

  changeSearchbar(event: any) {
    this.textShearch = event.detail.value;
    console.log(event.detail.value);

    this.filterResources();
  }
  cancelShearchbar() {
    this.modalController.dismiss();
  }

  filterResources() {
    this.resourcesMatch = null;
    if (isNullOrUndefined(this.textShearch)) { this.resourcesMatch = null; }
    else {
      if (this.textShearch.length === 0) { this.resourcesMatch = null; }
      else {
        if (this.resourceTypeId == 2) {
          this.resourcesMatch = this.resources.filter(x => { return x.content.toLowerCase().includes(this.textShearch.toLowerCase()) });
        } else {
          this.resourcesMatch = this.resources.filter(x => { return x.name.toLowerCase().includes(this.textShearch.toLowerCase()) });
        }

      }
    }

  }

  goToResourceDetail(resourceId: number, typeResourceId: number) {
    this.modalController.dismiss();
    this.navController.navigateForward(`/resource-detail/${resourceId}/${typeResourceId}`);
  }
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceSearchPage } from './resource-search.page';

describe('ResourceSearchPage', () => {
  let component: ResourceSearchPage;
  let fixture: ComponentFixture<ResourceSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceSearchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

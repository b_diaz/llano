import { Component, OnInit } from '@angular/core';
import { Resource } from 'src/app/models/Resource';
import { ModalController, LoadingController } from '@ionic/angular';
import { ResourceService } from 'src/app/services/resource.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resource-list',
  templateUrl: './resource-list.page.html',
  styleUrls: ['./resource-list.page.scss'],
})
export class ResourceListPage implements OnInit {

  namePage: string;
  resources: Resource[];
  typeId: number;
  error: boolean;
  loading: HTMLIonLoadingElement;

  constructor(
    public modalCtrl: ModalController,
    private resourceService: ResourceService,
    public loadingController: LoadingController,
    private route: ActivatedRoute
  ) {
    this.error = false;
  }

  async ngOnInit(): Promise<void> {
    this.typeId = parseInt(this.route.snapshot.paramMap.get('typeId'), 10);
    switch (this.typeId) {
      case 1:
        this.namePage = 'Palabras';
        break;
      case 2:
        this.namePage = 'Refranes';
        break;
      case 3:
        this.namePage = 'Coplas';
        break;
      case 4:
        this.namePage = 'Leyendas';
        break;

      default:
        break;
    }

    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: 'Por favor espere ..',
    });
    await this.loading.present();
    this.ionViewWillEnter();
  }

  ionViewWillEnter() {
    this.resourceService.getAllResourcesForTypeId(this.typeId).pipe(
    ).subscribe(
      (res: Resource[]) => {
        this.error = false;
        this.resources = res;
        console.log(this.resources);
       this.loading.dismiss();
      }, error => {
        console.error(error);
        this.resources = null;
        this.error = true;
        this.loading.dismiss();
      }
    );
  }


  doRefresh(ev: any) {
    this.ionViewWillEnter();
    setTimeout(() => {
      console.log('Refrescando');
      ev.target.complete();
    }, 3000);
  }

  ionViewWillLeave() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async viewNotification() {
    /*
    const modal = await this.modalCtrl.create({
      component: AppNotificationPage,
      componentProps: {
        prop1: "value",
        prop2: "value2"
      },
      cssClass: "modal-notification"
    });
    return await modal.present();
    */
  }

}

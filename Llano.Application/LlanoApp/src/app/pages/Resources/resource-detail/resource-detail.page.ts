import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Resource } from 'src/app/models/Resource';
import { ResourceService } from 'src/app/services/resource.service';
import { LoadingController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-resource-detail',
  templateUrl: './resource-detail.page.html',
  styleUrls: ['./resource-detail.page.scss'],
})
export class ResourceDetailPage implements OnInit {

  namePage: string;
  resource: Resource;
  resourceId: number;
  typeId: number;
  error: boolean;
  loading: HTMLIonLoadingElement;
  urlMedia: string;

  audioRequired: boolean;

  constructor(
    private route: ActivatedRoute,
    private resourceService: ResourceService,
    public loadingController: LoadingController
  ) {
    this.audioRequired = false;
    this.error = false;
    this.urlMedia = environment.urlMedia;
  }

  async ngOnInit(): Promise<void> {
    this.resourceId = parseInt(this.route.snapshot.paramMap.get('resourceId'), 10);
    this.typeId = parseInt(this.route.snapshot.paramMap.get('typeId'), 10);

    switch (this.typeId) {
      case 1:
        this.namePage = 'Palabra';
        break;
      case 2:
        this.namePage = 'Refran';
        break;
      case 3:
        this.namePage = 'Copla';
        break;
      case 4:
        this.namePage = 'Leyenda';
        this.audioRequired = true;
        break;

      default:
        break;
    }
    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: 'Por favor espere ..',
    });
    await this.loading.present();
    this.ionViewWillEnter();
  }

  ionViewWillEnter() {
    this.resourceService.getResourceForId(this.resourceId).pipe(
    ).subscribe(
      (res: Resource) => {
        this.error = false;
        this.resource = res;
        console.log(this.resource);
        this.loading.dismiss();
      }, error => {
        console.error(error);
        this.resource = null;
        this.error = true;
        this.loading.dismiss();
      }
    );
  }

  ionViewWillLeave() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  onSubmit() {

  }

}

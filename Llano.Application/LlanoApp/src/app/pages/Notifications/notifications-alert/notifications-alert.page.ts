import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Notification } from 'src/app/models/notification';

@Component({
  selector: 'app-notifications-alert',
  templateUrl: './notifications-alert.page.html',
  styleUrls: ['./notifications-alert.page.scss'],
})
export class NotificationsAlertPage implements OnInit {

  @Input() notification: Notification;

  constructor(
    public modalCtrl: ModalController,
    private navController: NavController
  ) { }

  ngOnInit() {
    console.log('Los datos que ingresan a notification alert son los siguientes', this.notification);
  }

  close() {
    this.modalCtrl.dismiss();
  }


}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UserRegistration } from 'src/app/models/UserRegistration';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { LoadingController, ToastController, NavController, AlertController, MenuController } from '@ionic/angular';
import { Location } from '@angular/common';
import { Storage } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoginModel } from 'src/app/models/LoginModel';
import { FcmService } from 'src/app/services/fcm.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit, OnDestroy {

  @ViewChild('signupSlider', { static: false }) signupSlider;

  indexSlides: number = 0;
  documentTypeList: DocumentType[];
  userRegistration: UserRegistration;
  type: string = "password";
  loading: HTMLIonLoadingElement;
  title: string = "Registro";
  loadingInit: HTMLIonLoadingElement;

  userGoogle: any;


  // Primer nombre
  firstName = new FormControl('', Validators.required);
  lastName = new FormControl('', Validators.required);
  phoneNumber = new FormControl('', Validators.required);
  AcceptTerms = new FormControl(false, Validators.required);

  slideOpts = {
    initialSlide: 0,
    allowTouchMove: false,
    autoHeight: true
  };

  public personForm: FormGroup;
  public notificationForm: FormGroup;
  public passwordForm: FormGroup;

  public submitAttempt: boolean = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private navContoller: NavController,
    private alertController: AlertController,
    private menuContoller: MenuController,
    private location: Location,
    private storage: Storage,
    private googlePlus: GooglePlus,
    private firebase: FcmService,
    private userService: UserService,
  ) {

    this.notificationForm = this.fb.group({
      allowMobileNotifications: [true],
      allowEmailNotifications: [true]
    })

  }

  async ngOnInit() {
    this.storage.get('userGoogle').then(
      (res: any) => {
        this.userGoogle = res,
          this.firstName.setValue(this.userGoogle.givenName)
        this.lastName.setValue(this.userGoogle.familyName)
      }, error => {
        console.error(error);
      })
  }

  back() {
    if (this.indexSlides == 0) {
      this.location.back();
    } else {
      this.prev();
    }
  }

  //Metodo de sliders
  next() {
    this.signupSlider.slideNext();
  }
  prev() {
    this.signupSlider.slidePrev();
  }

  slideChanged() {
    this.signupSlider.getActiveIndex().then(
      (res: number) => {
        this.indexSlides = res;
      }
    );
  }


  async groupForms() {

    this.userRegistration = {
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      phoneNumber: this.phoneNumber.value,
      email: this.userGoogle.email,
      password: this.userGoogle.userId,
      allowEmailNotifications: this.notificationForm.controls.allowMobileNotifications.value,
      allowMobileNotifications: this.notificationForm.controls.allowEmailNotifications.value,
    };

    let loginModel: LoginModel = {
      username: this.userGoogle.email,
      password: this.userGoogle.userId
    }

    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: 'Por favor espere ...'
    });
    this.loading.present();

    this.authService.register(this.userRegistration).subscribe(
      async res => {
        this.loading.message['cambio de mensaje'];
        this.login(loginModel);
      },
      async (error: any) => {
        let msm: string = "El Correo especificado ya está registrado, intenta la opción de recuperar contraseña"

        if (error.error && error.error == msm) {
          this.loading.dismiss();
          await this.presentAlertError();
          this.navContoller.navigateRoot(['/login']);

        } else {
          let toast = await this.toastController.create({
            message: 'error al crear usuario, ' + error.message,
            duration: 3000
          });
          this.loading.dismiss();
          toast.present();
        }
        console.error("error", error)
      }
    )
  }

  async presentAlertError() {
    const alert = await this.alertController.create({
      subHeader: 'Cuenta duplicada',
      message: 'El Correo ' + this.userRegistration.email + ', ya está registrado, Debes iniciar sesiòn',
      cssClass: 'alertCustom',
      buttons: ['OK']
    });

    await alert.present();
  }


  async presentAlertOk() {
    const alert = await this.alertController.create({
      header: 'Bienvenido a LlanoApp',
      subHeader: "Iniciativa que busca proteger nuestro lexico llanero",
      message: '¡Empecemos!',
      cssClass: 'alertCustom',
      buttons: ['OK']
    });

    await alert.present();
  }


  login(loginModel: LoginModel) {
    this.authService.getToken(loginModel).subscribe(
      async (res: any) => {
        console.log("Respuesta del getToken", res);
        await this.storage.set('AuthData', res)
        this.storage.set('LoginForm', loginModel);
        this.getUserAndinitFuncionsFirebase(loginModel.username);

        this.loading.dismiss();
        this.navContoller.navigateRoot(['/tabs'])
        //toast.present();
      }, async (error: any) => {
        if (error.error) {
          console.log("Imprimiendo error en login", error);

          if (error.error.error_description == "invalid_username_or_password") {
            console.log("invalid_username_or_password");

            this.loading.dismiss();
          } else {
            let toast = await this.toastController.create({ message: 'Error de conexión', duration: 5000 })
            console.error("parece que algo salio mal recuperando el token del usuario", error)
            this.loading.dismiss();
            toast.present();
          }
        } else {
          let toast = await this.toastController.create({ message: 'Error iniciando sesion, ' + error.message, duration: 5000, cssClass: 'toastNotification' })
          this.loading.dismiss();
          console.error("parece que algo salio mal recuperando el token del usuario", error)
          toast.present();
        }

      }
    );
  }

  getUserAndinitFuncionsFirebase(email: string) {
    this.userService.getCurrentUser(email).subscribe(
      (res: any) => {
        this.storage.set('idUser', res.id);
        this.initFuncionsFirebase(res.id);

        console.log("Imprimiendo usuario actual en login", res);
      }, error => {
        console.error("Error al intentar obtener usuarioActual en login");
      }
    )
  }


  initFuncionsFirebase(idUser: string) {
    this.firebase.subscribeTopicUser(idUser);
  }

  ngOnDestroy() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}


import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  user: any;

  constructor(
    private googlePlus: GooglePlus,
    private httpClient: HttpClient,
    private storage: Storage,
    private navController: NavController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  google() {
    this.googlePlus.login({
      'scopes': 'openid',
      'webClientId': '12755649852-j1r0r3v1gue3k7mp2s0f7loh8aru7aj6.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'offline': true // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
    })
      .then(async (res: any) => {
        console.log(res);
        await this.storage.set('userGoogle', res);
        this.navController.navigateForward('register');

        /*
        let data =
          'code=' + res.serverAuthCode
        '&client_id=12755649852-j1r0r3v1gue3k7mp2s0f7loh8aru7aj6.apps.googleusercontent.com' +
          '&client_secret=Pu-eqeiZnGfdmOga-tTpO7rl' +
          '&grant_type=authorization_code';

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        let options = { headers: headers };

        this.httpClient.post('https://oauth2.googleapis.com/token', encodeURI(data.toString()), options).subscribe(
          async (res: any) => {
            await this.nativeStorage.setItem('userGoogle', res);
            this.navController.navigateForward('register');
            console.log(res)
          }, error => {
            console.log("Error", error);
            const toast = this.toastController.create({
              message: ' Error al descargar los datos'
            })
          }
          
        )
      */
      })
      .catch(err => {
        console.error(err);
        this.googlePlus.disconnect();
      });
  }

}

import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, ToastController } from '@ionic/angular';
import { Location } from '@angular/common';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginModel } from 'src/app/models/LoginModel';
import { AuthService } from 'src/app/services/auth.service';
import { Storage } from '@ionic/storage';
import { UserService } from 'src/app/services/user.service';
import { FcmService } from 'src/app/services/fcm.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  avatarimage: any;
  signform: any;
  flashCardFlipped: boolean = false;
  loading: HTMLIonLoadingElement;
  alert: HTMLIonAlertElement;

  constructor(
    public navCtrl: NavController,
    private location: Location,
    private googlePlus: GooglePlus,
    private httpClient: HttpClient,
    private loadingController: LoadingController,
    private authService: AuthService,
    private storage: Storage,
    private alertController: AlertController,
    private toastController: ToastController,
    private userService: UserService,
    private firebase: FcmService
  ) {

  }

  ngOnInit() {
    this.avatarimage = '../../../../assets/img/femaleRight.svg';
    this.signform = "login"
  }

  changeAvatar() {
    if (this.signform == 'login') {
      this.avatarimage = '../../../../assets/img/femaleRight.svg';
    }

    if (this.signform == 'signup') {
      this.avatarimage = '../../../../assets/img/femaleLeft.svg';
    }
  }

  back() {
    this.location.back();
  }

  google() {
    this.googlePlus.login({
      // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'webClientId': '12755649852-j1r0r3v1gue3k7mp2s0f7loh8aru7aj6.apps.googleusercontent.com',
      // optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      'offline': true
    }).then(async (res: any) => {
      console.log('res', res);

      let loginModel: LoginModel = {
        username: res.email,
        password: res.userId
      }

      console.log("este es el server code que envia ", res.serverAuthCode);

      // this.getAccessToken(res.serverAuthCode, loginModel);
      await this.login(loginModel);

    }).catch(
      err => {
        console.error(err),
          console.log("Aqui ccancelo el login");

        this.googlePlus.logout();
      }

    );
  }

  getAccessToken(serverAuthCode: string, loginModel: LoginModel) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      })
    };

    const data =
      'code=' + serverAuthCode +
      '&client_id=12755649852-j1r0r3v1gue3k7mp2s0f7loh8aru7aj6.apps.googleusercontent.com&' +
      'client_secret=Pu-eqeiZnGfdmOga-tTpO7rl&' +
      // 'redirect_uri=https://localhost:5200/code&' + 
      'grant_type=authorization_code';




    this.httpClient.post('https://oauth2.googleapis.com/token', encodeURI(data.toString()), httpOptions).subscribe(
      async (res: any) => {

        console.log("respuesta del getAccessToken", res);
        // await this.login(loginModel);

      }, error => {
        console.log('Error del getAccessToken', error);
        this.googlePlus.logout();
      });

  }

  async login(res: LoginModel) {

    let loginModel: LoginModel = {
      username: res.username, //"brad1diaz@gmail.com", 
      password: res.password //"116699849739507387020"
    }

    this.loading = await this.loadingController.create({
      spinner: null,
      cssClass: 'custom-loader',
      message: "Cargando ..."
    });
    this.loading.present();

    this.authService.getToken(loginModel).subscribe(
      async (res: any) => {
        console.log("Respuesta del getToken", res);
        await this.storage.set('AuthData', res)
        this.storage.set('LoginForm', loginModel);
        this.getUserAndinitFuncionsFirebase(loginModel.username);

        this.loading.dismiss();
        this.navCtrl.navigateRoot(['/tabs'])
        //toast.present();
      }, async (error: any) => {
        if (error.error) {
          console.log("Imprimiendo error en login", error);

          if (error.error.error_description == "invalid_username_or_password") {
            this.loading.dismiss();
            this.presentAlert();
          } else {
            let toast = await this.toastController.create({ message: 'Error de conexión', duration: 5000 })
            console.error("parece que algo salio mal recuperando el token del usuario", error)
            this.loading.dismiss();
            toast.present();
          }
        } else {
          let toast = await this.toastController.create({ message: 'Error iniciando sesion, ' + error.message, duration: 5000, cssClass: 'toastNotification' })
          this.loading.dismiss();
          console.error("parece que algo salio mal recuperando el token del usuario", error)
          toast.present();
        }

      }
    );
    // }
  }

  async presentAlert() {
    this.googlePlus.disconnect().then(
      res => {
        console.log("Imprimiendo respuesta", res);
      },
      error => {
        console.error("Imprimiendo error", error);
      }
    )

    this.alert = await this.alertController.create({
      header: 'Usuario No registrado',
      message: 'Para iniciar como colaborador debes estar registrado.',
      cssClass: 'alertCustom',
      buttons: ['OK']
    });

    await this.alert.present();
  }

  getUserAndinitFuncionsFirebase(email: string) {
    this.userService.getCurrentUser(email).subscribe(
      (res: any) => {
        this.storage.set('idUser', res.id);
        this.initFuncionsFirebase(res.id);

        console.log("Imprimiendo usuario actual en login", res);
      }, error => {
        console.error("Error al intentar obtener usuarioActual en login");

      }
    )
  }

  initFuncionsFirebase(idUser: string) {
    this.firebase.subscribeTopicUser(idUser);
  }


}

import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs';
import { FcmService } from './services/fcm.service';
import { Router } from '@angular/router';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Location } from '@angular/common';
import { defineCustomElements } from 'gl-ionic-background-video/dist/loader';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  showSplash: boolean = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _firebase: FcmService,
    private _router: Router,
    private appMinimize: AppMinimize,
    private navController:NavController,
    private location: Location,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.storage.get('LoginForm').then(
        (res:any) => {
          console.log("Intenta obtenr informacion del loginForm", res);          
          if (res) {
            this.navController.navigateRoot(['/tabs/home']);
          }else{
            this.navController.navigateRoot(['/welcome']);
          }
        }
      )
      //this.statusBar.styleDefault();
      defineCustomElements(window);
      this.statusBar.backgroundColorByHexString('#00a82d')
      this.backButonListen();
      this._firebase.getToken();
      this._firebase.onNotificationOpen();
      this.splashScreen.hide();    
      timer(3000).subscribe(() => {        
        this.showSplash = false;
      }) 
      this.resumeApp();
    });
  }

  backButonListen() {
    console.log("Ingresa a backButonListen")
    this.platform.backButton.subscribe(
      res => {
        console.log("Escucha evento de backButon")
        let url = this._router.url;
        if (url == '/home' || url == '/tabs/home' || url == '/welcome') {
          this.appMinimize.minimize();
        }
        
        if (url == '/tabs/media'|| url == '/tabs/profile' ) {
          this.navController.navigateRoot(['/tabs/home']);
        }

        if( url.includes('resource-detail/') || url.includes('resource-list/')){
          this.back()
        }

      });
  }

  initFuncionsFirebase() {
    this._firebase.onNotificationOpen();

  }

  resumeApp(){
    this.platform.resume.subscribe(
      res => {
        this._firebase.clearAllNotifications();
      },error => {
        console.error("Error en evento resume");        
      }
    )
  }

  back() {
    this.location.back();
  }
}

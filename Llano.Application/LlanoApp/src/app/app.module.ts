import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResourceService } from './services/resource.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ResourceSearchPageModule } from './pages/Resources/resource-search/resource-search.module';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpInteceptor } from './httpInterceptors/http-inteceptor';
import { IonicStorageModule } from '@ionic/storage';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { NotificationsAlertPage } from './pages/Notifications/notifications-alert/notifications-alert.page';
import { NotificationsAlertPageModule } from './pages/Notifications/notifications-alert/notifications-alert.module';

import 'gl-ionic-background-video';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [NotificationsAlertPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ResourceSearchPageModule,
    NotificationsAlertPageModule,
    IonicStorageModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    StatusBar,
    SplashScreen,
    ResourceService,
    FileChooser,
    File,
    FilePath,
    FileTransfer,
    FileTransferObject,
    Media,
    GooglePlus,
    AppMinimize,
    FirebaseX,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInteceptor,
      multi: true
    }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  welcome: any;

  constructor(
    private storage: Storage,
    private navController: NavController,
  ) { }

  async ngOnInit() {
    this.welcome = await this.storage.get("LoginForm")
  }


  goToHome(){
    this.navController.navigateRoot(['/welcome']);
  }

}

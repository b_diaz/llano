import { NgModule } from '@angular/core';
import { FirstWordPipe } from './first-word.pipe';
import { FilterResourcesPipe } from './filter-resources.pipe';



@NgModule({
  declarations: [FirstWordPipe, FilterResourcesPipe],
   exports: [
    FirstWordPipe,
    FilterResourcesPipe
  ]
})
export class PipesModule { }

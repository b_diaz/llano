import { Pipe, PipeTransform } from '@angular/core';
import { Resource } from '../models/Resource';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'filterResources'
})
export class FilterResourcesPipe implements PipeTransform {

  transform(resources: Resource[], texto: string): Resource[] {
    console.log('en el pipe recibe los resources', resources);
    console.log('recibe de texto : ',texto);
    
    if(isNullOrUndefined(texto)) { return null }
    if(texto.length === 0) { return null }
    

   return resources.filter(x => {return x.name.toLowerCase().includes(texto.toLowerCase())});

   

  }

}

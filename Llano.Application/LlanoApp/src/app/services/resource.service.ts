import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Resource } from '../models/Resource';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  baseUrl: string;
  listResources: Resource[];
  resourcesSubject =  new BehaviorSubject<Resource[]>(this.listResources);

  constructor(
    private http: HttpClient
  ) {
    this.baseUrl = environment.urlApi;
  }

  getAllResourcesForTypeId(typeId: number): Observable<Resource[]> {
    const callUrl = this.baseUrl + '/Resource/Type/' + typeId;
    return this.http.get<Resource[]>(callUrl);
  }

  sendResources(listResources: Resource[]) {
    this.resourcesSubject.next(listResources);
  }

  getResources(): Observable<Resource[]> {
    return this.resourcesSubject.asObservable();
  }

  getResourceForId(resourceId: number) {
    const callUrl = this.baseUrl + '/Resource/' + resourceId;
    return this.http.get<Resource>(callUrl);
  }

  createResource(formResource: FormData) {
    const callUrl = this.baseUrl + '/Resource';
    return this.http.post(callUrl, formResource);
  }

  getResourcesThisUser(UserApplicationId: string) {
    const callUrl = this.baseUrl + '/Resource/User/' + UserApplicationId;
    return this.http.get<Resource[]>(callUrl);
  }
}

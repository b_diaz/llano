import { Injectable } from '@angular/core';
import { Platform, ToastController, ModalController } from '@ionic/angular';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { NotificationsAlertPage } from '../pages/Notifications/notifications-alert/notifications-alert.page';

@Injectable({
  providedIn: 'root'
})
export class FcmService {
  constructor(
    private _firebase: FirebaseX,
    private _platform: Platform,
    private _toast: ToastController,
    private storage: Storage,
    private modalController: ModalController,
    private router: Router

  ) { }

  getToken() {
    this._firebase.getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));
  }

  onNotificationOpen() {
    this._firebase.onMessageReceived()
      .subscribe(
        (data: any) => {
          console.log("Nueva notificacion ", data);

          if (data.dataRefresh) {
            console.log("Activa eventos en background");
          } else {
            if (data.tap) {
              var dataConvert =  {
                title: data.title,
                body: data.body
              }

              console.log("Se recibio en segundo plano", data);
              this.notificationFont(dataConvert);
            } else {
              console.log("Se recibe en primer plano", data);
              this.notificationFont(data);
            }
          }

        }, error => {
          console.error('onNotificationOpen error', error);
        }

      );
  }

  async notificationFont(data: any) {

    const modal = await this.modalController.create({
      component: NotificationsAlertPage,
      componentProps: {
        'notification': data,
      },
      cssClass: "modal-multimedia2",
    });

    return await modal.present();
  }

  onTokenRefresh() {
    this._firebase.onTokenRefresh()
      .subscribe((token: string) => console.log(`Got a new token ${token}`));
  }

  clearAllNotifications() {
    this._firebase.clearAllNotifications().then(
      res => {
        console.log("clearAllNotifications ok");
      }, error => {
        console.error("clearAllNotifications Error", error);
      }
    )
  }


  subscribeTopicUser(idUser: string) {
    this._firebase.subscribe(idUser).then(
      res => {
        console.log("registerTopicUser exitoso ");

      }, error => {
        console.error("registerTopicUser error");

      }
    )

  }

  unsubscribeTopicUser(idUser: string) {
    this._firebase.unsubscribe(idUser).then(
      res => {
        console.log("unsubscribeTopicUser exitoso");
      }, error => {
        console.error("unsubscribeTopicUser Error", error);
      }
    )
  }
}

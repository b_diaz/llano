import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserApplication } from '../models/UserApplication';
import { Observable } from 'rxjs';
import { UserViewModel } from '../models/UserViewModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  urlAuth: string;
  urlBase: string;

  constructor(
    private http: HttpClient
  ) {
    this.urlAuth = environment.urlAuth;
  }

  getCurrentUser(email: string): Observable<UserApplication> {
    var callUrl = this.urlAuth + '/api/user/getByEmail/' + email;
    return this.http.get<UserApplication>(callUrl);
  }

  getInfo(idUser: string): Observable<UserViewModel> {
    var callUrl = this.urlAuth + '/api/user/getUser/' + idUser;
    return this.http.get<UserViewModel>(callUrl);
  }

}

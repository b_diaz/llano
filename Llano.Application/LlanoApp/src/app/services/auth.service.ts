import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { from, throwError } from 'rxjs';
import { switchMap, flatMap } from 'rxjs/operators';
import { AlertController, ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { LoginModel } from '../models/LoginModel';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private urlAuth: string = environment.urlAuth;
    private urlApi: string = environment.urlApi;
    constructor(
        private _http: HttpClient,
        private _storage: Storage,
        public _alert: AlertController,
        private _router: Router,
        private _toast: ToastController,
        private _nav: NavController
    ) { }


    getToken(loginModel: LoginModel) {

        let url = this.urlAuth + '/connect/token';

        let dataLoging =
            "username=" + loginModel.username +
            "&password=" + loginModel.password +
            "&client_id=" + 'mobile.LlanoApp.com.co' +
            "&client_secret=" + 'secret' +
            "&grant_type=" + 'password' +
            "&scope=" + 'openid profile phone email api.read offline_access roles'

        let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        let options = { headers: headers };

        return this._http.post(url, encodeURI(dataLoging.toString()), options);

    }

    getRefreshToken() {
        let promise = this._storage.get("AuthData")
        var observableFromPromise = from(promise);

        let url = this.urlAuth + '/connect/token';

        return observableFromPromise.pipe(
            switchMap((res: any) => {
                console.log("Imprimiendo observableFromPromise, cuando intenta obtener el refreshToken", res);

                //si llega  este punto es por que no encontro el token
                //asi que si no tiene el refresh token debe logearse nuevamente
                if (isNullOrUndefined(res)) {
                    this.presentAlert();
                    return throwError("El Usario debe Iniciar sesion Nuevamente");
                }
                let authData = res;

                //en este punto si cuenta con el token pero al parecer este ya no es valido

                let promise2 = this._storage.get("LoginForm")
                var observableFromPromise2 = from(promise2);

                return observableFromPromise2.pipe(
                    flatMap(
                        (loginForm: any) => {
                            let data =
                                "username=" + loginForm.UserName +
                                "&password=" + loginForm.Password +
                                "&client_id=" + 'mobile.LlanoApp.com.co' +
                                "&client_secret=" + 'secret' +
                                "&grant_type=" + 'refresh_token' +
                                "&scope=" + 'openid profile phone email api.read offline_access roles' +
                                "&refresh_token=" + authData.refresh_token;

                            let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
                            let options = { headers: headers };

                            return this._http.post(url, encodeURI(data.toString()), options);

                        }
                    )
                )
            })
        )
    }



    async presentAlert() {
        const alert = await this._alert.create({          //header: 'Seccion Cerrada',

            header: 'Sesión cerrada',
            subHeader: 'La sesión a sido cerrado, inicia nuevamente',
            message: 'Este tipo de cierre suele darse por la eliminacion de los datos de la aplicacion',
            buttons: [{
                text: 'Ok',
                cssClass: 'warning',
                handler: () => {
                    this._nav.navigateRoot(['/welcome']);
                },

            }],
            backdropDismiss: false,
            cssClass: 'alertCustom'
        });

        await alert.present();
    }


    register(userRegistration: any) {
        return this._http.post(this.urlAuth + '/api/account', userRegistration);
    }
    
}
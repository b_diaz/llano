export const environment = {
  //ngrok http 60329 -host-header="localhost:60329"
  production: true,
  urlApi: 'https://www.llanoapp.studio/api',
  urlMedia: 'https://www.llanoapp.studio',
  urlAuth: 'https://www.auth.llanoapp.studio'
};

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Llano.Interfaces
{
    /// <summary>
    /// Define las funcionalidades para acceso a la información de las rutas (Directorios)
    /// </summary>
    public interface IPathProvider
    {
        /// <summary>
        /// Raiz del Servidor (Directorio Raiz de la Aplicación)
        /// </summary>
        string WebRootPath { get; }
        /// <summary>
        /// Proceso para mapear la ruta a una ruta relativa del servidor
        /// </summary>
        /// <param name="path">Ruta relativa</param>
        /// <returns>Ruta completa</returns>
        string MapPath(string path);
    }
}

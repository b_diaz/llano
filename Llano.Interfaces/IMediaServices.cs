﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Interfaces
{
    public interface IMediaServices
    {
        Task<string> SaveFile(Stream fileStreamIn, string filePathIn, string fileNameIn);
    }
}

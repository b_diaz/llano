﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Llano.Interfaces
{
    public interface IHistoryResource
    {
        void CreateHistoryResource(HistoryResource historyResource);
    }
}

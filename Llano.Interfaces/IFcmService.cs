﻿using Llano.Models;
using Llano.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Interfaces
{
    public interface IFcmService
    {
        Task<string> SendNotification(Notification notification);
        Task<string> Send(MessageViewModel message);


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Llano.Models;

namespace Llano.Interfaces
{
    public interface IEmailSender
    {
        bool SendEmailResetPassword(string destinationEmail, string actionUrl, string nameUser);
        bool SendEmailWelcome(string destinationEmail, string actionUrl, string nameUser);
        
        /*
         * bool SendEmailPasswordReset(string destinationEmail, string callbackUrl);

        bool SendEmailConfirmation(string destinationEmail, string assignedPassword, string callbackUrl, bool isFacebookUser = false);
        bool SendEmailConfirmation(string destinationEmail, string callbackUrl);
    */
    }
}

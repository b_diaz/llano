﻿using Llano.Models;
using Llano.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Interfaces
{
    public interface IUserServices
    {
        Task<UserViewModel> GetUser(string idUser);
        Task<Customer> GetUserByEmail(string email);
    }
}

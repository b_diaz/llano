﻿using Llano.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Llano.Interfaces
{
    public interface IResourceService
    {
        #region ---------- GET -----------
        Task<List<Resource>> GetAll();
        Task<List<Resource>> GetForType(int TypeResourceId);
        Task<List<Resource>> GetForTypeAndContain(int TypeResourceId, string Contain);
        Task<Resource> GetForId(long ResourceId);
        Task<List<Resource>> GetForUserId(string UserApplicationId);
        Task<List<Resource>> GetForUserAndTypeAndStatus(string UserApplicationId, int TypeResourceId, int StatusResourceId);
        Task<List<Resource>> GetForSatus(int StatusResourceId);
        Task<List<HistoryResource>> HistoryResource(long ResourceId);
        Task<List<Resource>> ResourcesWithoutRead();

        #endregion

        #region ------------ POST -----------
        void CreateResource(Resource resource);
        #endregion

        #region ------------ PUT ---------------
        Task Update(long idResource, Resource resource);

        Task MarkAsRead(long idResource);

        #endregion

        #region --------- DELETE -----------
        Task Delete(long idResource);
        #endregion
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Llano.DataAccess.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    AllowMobileNotifications = table.Column<bool>(nullable: false),
                    AllowEmailNotifications = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StatusResource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatusResource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeResource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeResource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Resource",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Audio = table.Column<string>(nullable: true),
                    Read = table.Column<bool>(nullable: false),
                    Disable = table.Column<bool>(nullable: false),
                    TypeResourceId = table.Column<int>(nullable: false),
                    StatusResourceId = table.Column<int>(nullable: false),
                    UserApplicationId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resource", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resource_StatusResource_StatusResourceId",
                        column: x => x.StatusResourceId,
                        principalTable: "StatusResource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Resource_TypeResource_TypeResourceId",
                        column: x => x.TypeResourceId,
                        principalTable: "TypeResource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Resource_AspNetUsers_UserApplicationId",
                        column: x => x.UserApplicationId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HistoryResource",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    StatusResourceId = table.Column<int>(nullable: false),
                    ResourceId = table.Column<long>(nullable: false),
                    UserApplicationId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoryResource", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistoryResource_Resource_ResourceId",
                        column: x => x.ResourceId,
                        principalTable: "Resource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HistoryResource_StatusResource_StatusResourceId",
                        column: x => x.StatusResourceId,
                        principalTable: "StatusResource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_HistoryResource_AspNetUsers_UserApplicationId",
                        column: x => x.UserApplicationId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "AllowEmailNotifications", "AllowMobileNotifications", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UpdatedOn", "UserName" },
                values: new object[,]
                {
                    { "1", 0, true, true, "1d03f933-fdc4-4251-82c6-03115976214d", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(569), "Wilsen@gmail.com", false, "Wilsen Humberto", "Duarte cibo", false, null, null, null, null, "1231231", false, null, false, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(574), null },
                    { "2", 0, true, true, "ca1d5158-ba2d-4355-8c6e-a45dcecaad3a", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6521), "Breidy@gmail.com", false, "Breidy Alexis", "Acevedo Diaz", false, null, null, null, null, "1231231", false, null, false, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6527), null },
                    { "3", 0, true, true, "6d22483f-cd38-4007-a734-71477ee66195", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6733), "Arnol@gmail.com", false, "Arnol Sebastian", "Moreno Serrano", false, null, null, null, null, "1231231", false, null, false, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6735), null }
                });

            migrationBuilder.InsertData(
                table: "StatusResource",
                columns: new[] { "Id", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(483), "Solicitado", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(489) },
                    { 2, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1130), "Descartado", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1136) },
                    { 3, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1144), "Aprobado", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1145) }
                });

            migrationBuilder.InsertData(
                table: "TypeResource",
                columns: new[] { "Id", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 1, 11, 12, 51, 50, 509, DateTimeKind.Local).AddTicks(6925), "Palabras", new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(3579) },
                    { 2, new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5676), "Refranes", new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5688) },
                    { 3, new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5701), "Coplas", new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5702) },
                    { 4, new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5704), "Leyendas", new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5705) }
                });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Audio", "Content", "CreatedOn", "Disable", "Name", "Read", "StatusResourceId", "TypeResourceId", "UpdatedOn", "UserApplicationId" },
                values: new object[,]
                {
                    { 1L, null, "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(4373), false, "Palabra 1", false, 1, 1, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(5015), "1" },
                    { 2L, null, "Pa qué se limpia las patas quien va a dormir en el suelo", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8727), false, "Palabra 2", false, 1, 1, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8735), "1" },
                    { 3L, null, "Loro con ala cortada es el que más aletea", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8766), false, "Palabra 3", false, 1, 1, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8767), "1" },
                    { 4L, null, "Borracho con real no estorba así arroje en la cantina", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8770), false, null, false, 2, 2, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8771), "2" },
                    { 5L, null, "Más guapo que el caballo de Bolívar (En el llano guapo es sinónimo de valentía y coraje) Le hecho palo a todo mogote.", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8774), false, null, false, 2, 2, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8774), "2" },
                    { 6L, null, "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8776), false, null, false, 2, 2, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8777), "2" },
                    { 7L, null, "Nunca se cae de la cama, el que en el suelo se acuesta", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8779), false, "Copla 1", false, 3, 3, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8780), "3" },
                    { 8L, null, "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8782), false, "Copla 2", false, 3, 3, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8783), "3" },
                    { 9L, null, "Quien va a ser amansador sin montar potro cerrero", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8785), false, "Copla 3", false, 3, 3, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8785), "3" },
                    { 10L, null, "Me dejaste como como rolo de gender leña", new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8787), false, "Copla 4", false, 3, 3, new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8788), "3" }
                });

            migrationBuilder.InsertData(
                table: "HistoryResource",
                columns: new[] { "Id", "CreatedOn", "Message", "ResourceId", "StatusResourceId", "UpdatedOn", "UserApplicationId" },
                values: new object[] { 1L, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(7960), null, 1L, 1, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(7965), "1" });

            migrationBuilder.InsertData(
                table: "HistoryResource",
                columns: new[] { "Id", "CreatedOn", "Message", "ResourceId", "StatusResourceId", "UpdatedOn", "UserApplicationId" },
                values: new object[] { 2L, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9466), null, 2L, 2, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9472), "2" });

            migrationBuilder.InsertData(
                table: "HistoryResource",
                columns: new[] { "Id", "CreatedOn", "Message", "ResourceId", "StatusResourceId", "UpdatedOn", "UserApplicationId" },
                values: new object[] { 3L, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9501), null, 3L, 3, new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9503), "3" });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_HistoryResource_ResourceId",
                table: "HistoryResource",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_HistoryResource_StatusResourceId",
                table: "HistoryResource",
                column: "StatusResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_HistoryResource_UserApplicationId",
                table: "HistoryResource",
                column: "UserApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_StatusResourceId",
                table: "Resource",
                column: "StatusResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_TypeResourceId",
                table: "Resource",
                column: "TypeResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_UserApplicationId",
                table: "Resource",
                column: "UserApplicationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "HistoryResource");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Resource");

            migrationBuilder.DropTable(
                name: "StatusResource");

            migrationBuilder.DropTable(
                name: "TypeResource");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Llano.DataAccess.Migrations
{
    public partial class userDisabled : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Disabled",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "ad78fb7b-a115-47af-82a4-af809eb14e40", new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(6161), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(6165) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "66323cf0-ed0f-449a-9cbf-4b8e7b7bcc33", new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(8631), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(8634) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "e6eac14d-b3c2-4f7b-9b51-ae9e854cfdb3", new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(8761), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(8762) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9172), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9174) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9176), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9176) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9177), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(9178) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4285), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4291) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4312), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4313) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4315), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4316) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4317), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4318) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4319), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4320) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4321), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4322) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4323), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4323) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4325), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4326) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4327), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4327) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4329), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(4329) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1168), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1170) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1174), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1175) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1176), new DateTime(2020, 6, 14, 20, 14, 4, 893, DateTimeKind.Local).AddTicks(1177) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(375), new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8755) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8772), new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8773) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8775), new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8776) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8778), new DateTime(2020, 6, 14, 20, 14, 4, 892, DateTimeKind.Local).AddTicks(8778) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Disabled",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "1d03f933-fdc4-4251-82c6-03115976214d", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(569), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(574) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "ca1d5158-ba2d-4355-8c6e-a45dcecaad3a", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6521), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6527) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "UpdatedOn" },
                values: new object[] { "6d22483f-cd38-4007-a734-71477ee66195", new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6733), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(6735) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(7960), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(7965) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9466), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9472) });

            migrationBuilder.UpdateData(
                table: "HistoryResource",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9501), new DateTime(2020, 1, 11, 12, 51, 50, 513, DateTimeKind.Local).AddTicks(9503) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(4373), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(5015) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8727), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8735) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8766), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8767) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8770), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8771) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8774), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8774) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8776), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8777) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 7L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8779), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8780) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 8L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8782), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8783) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 9L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8785), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8785) });

            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8787), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(8788) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(483), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(489) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1130), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1136) });

            migrationBuilder.UpdateData(
                table: "StatusResource",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1144), new DateTime(2020, 1, 11, 12, 51, 50, 512, DateTimeKind.Local).AddTicks(1145) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 509, DateTimeKind.Local).AddTicks(6925), new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(3579) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5676), new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5688) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5701), new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5702) });

            migrationBuilder.UpdateData(
                table: "TypeResource",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedOn", "UpdatedOn" },
                values: new object[] { new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5704), new DateTime(2020, 1, 11, 12, 51, 50, 511, DateTimeKind.Local).AddTicks(5705) });
        }
    }
}

﻿using Llano.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace Llano.DataAccess
{
    public class LlanoDbContext : IdentityDbContext<Customer>
    {
        public LlanoDbContext(DbContextOptions<LlanoDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            #region ------------- TypesResources ----------------
            modelBuilder.Entity<TypeResource>().HasData(
                  new TypeResource
                  {
                      Id = 1,
                      Name = "Palabras"
                      
                  },
                  new TypeResource
                  {
                      Id = 2,
                      Name = "Refranes"
                  },
                  new TypeResource
                  {
                      Id = 3,
                      Name = "Coplas"
                  },
                  new TypeResource
                  {
                      Id = 4,
                      Name = "Leyendas"
                  }
              );
            #endregion

            #region ------------- StatusResources ----------------
            modelBuilder.Entity<StatusResource>().HasData(
             new StatusResource
             {
                 Id = 1,
                 Name = "Solicitado"
             },
             new StatusResource
             {
                 Id = 2,
                 Name = "Descartado"
             },
             new StatusResource
             {
                 Id = 3,
                 Name = "Aprobado"
             }
         );

            #endregion

            #region ------------- Resources ----------------
            modelBuilder.Entity<Resource>().HasData(
             new Resource
             {
                 Id = 1,
                 Name = "Palabra 1",
                 Content = "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).",
                 StatusResourceId = 1,
                 TypeResourceId = 1,
                 UserApplicationId = "1"
             },
             new Resource
             {
                 Id = 2,
                 Name = "Palabra 2",
                 Content = "Pa qué se limpia las patas quien va a dormir en el suelo",
                 StatusResourceId = 1,
                 TypeResourceId = 1,
                 UserApplicationId = "1"
             },
             new Resource
             {
                 Id = 3,
                 Name = "Palabra 3",
                 Content = "Loro con ala cortada es el que más aletea",
                 StatusResourceId = 1,
                 TypeResourceId = 1,
                 UserApplicationId = "1"
             },
             new Resource
             {
                 Id = 4,
                 Content = "Borracho con real no estorba así arroje en la cantina",
                 StatusResourceId = 2,
                 TypeResourceId = 2,
                 UserApplicationId = "2"
             },
             new Resource
             {
                 Id = 5,
                 Content = "Más guapo que el caballo de Bolívar (En el llano guapo es sinónimo de valentía y coraje) Le hecho palo a todo mogote.",
                 StatusResourceId = 2,
                 TypeResourceId = 2,
                 UserApplicationId = "2"
             },
             new Resource
             {
                 Id = 6,
                 Content = "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).",
                 StatusResourceId = 2,
                 TypeResourceId = 2,
                 UserApplicationId = "2"
             },
             new Resource
             {
                 Id = 7,
                 Name = "Copla 1",
                 Content = "Nunca se cae de la cama, el que en el suelo se acuesta",
                 StatusResourceId = 3,
                 TypeResourceId = 3,
                 UserApplicationId = "3"
             },
             new Resource
             {
                 Id = 8,
                 Name = "Copla 2",
                 Content = "¿Por qué lo usamos? Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí,contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).",
                 StatusResourceId = 3,
                 TypeResourceId = 3,
                 UserApplicationId = "3"
             },
             new Resource
             {
                 Id = 9,
                 Name = "Copla 3",
                 Content = "Quien va a ser amansador sin montar potro cerrero",
                 StatusResourceId = 3,
                 TypeResourceId = 3,
                 UserApplicationId = "3"
             },
              new Resource
              {
                  Id = 10,
                  Name = "Copla 4",
                  Content = "Me dejaste como como rolo de gender leña",
                  StatusResourceId = 3,
                  TypeResourceId = 3,
                  UserApplicationId = "3"
              }

         );

            #endregion

            #region ------------- Application Users ------------
            modelBuilder.Entity<Customer>().HasData(
               new Customer
               {
                   Id = "1",
                   FirstName = "Wilsen Humberto",
                   LastName = "Duarte cibo",
                   PhoneNumber = "1231231",
                   Email = "Wilsen@gmail.com",
                   AllowMobileNotifications = true,
                   AllowEmailNotifications = true
               },
               new Customer
               {
                   Id = "2",
                   FirstName = "Breidy Alexis",
                   LastName = "Acevedo Diaz",
                   PhoneNumber = "1231231",
                   Email = "Breidy@gmail.com",
                   AllowMobileNotifications = true,
                   AllowEmailNotifications = true
               },
               new Customer
               {
                   Id = "3",
                   FirstName = "Arnol Sebastian",
                   LastName = "Moreno Serrano",
                   PhoneNumber = "1231231",
                   Email = "Arnol@gmail.com",
                   AllowMobileNotifications = true,
                   AllowEmailNotifications = true
               }
           );
            #endregion

            #region ------------- History Resources ------------
            modelBuilder.Entity<HistoryResource>().HasData(
               new HistoryResource
               {
                   Id = 1,
                   ResourceId = 1,
                   StatusResourceId = 1,
                   UserApplicationId = "1"
               },
               new HistoryResource
               {
                   Id = 2,
                   ResourceId = 2,
                   StatusResourceId = 2,
                   UserApplicationId = "2"
               },
               new HistoryResource
               {
                   Id = 3,
                   ResourceId = 3,
                   StatusResourceId = 3,
                   UserApplicationId = "3"
               }
           );
            #endregion

        }
    }
}

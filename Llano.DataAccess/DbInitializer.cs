﻿using Llano.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Llano.DataAccess
{
    public class DbInitializer
    {
        public static void SeedRoles(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                try
                {


                    var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                                                         
                    var roles = new List<IdentityRole>
                    {
                        new IdentityRole() { Name = "Admin" },
                        new IdentityRole() { Name = "Collaborator" }
                    };

                    foreach (var role in roles)
                    {
                        if (!roleManager.RoleExistsAsync(role.Name).Result)
                        {
                            var roleResult = roleManager.CreateAsync(role).Result;
                       }
                    }


                    var _userManager = serviceProvider.GetRequiredService<UserManager<Customer>>();

                    var AdminUser = new Customer()
                    {
                        FirstName = "Admin",
                        LastName = "LlanoApp",
                        Email = "LlanoApp@llanoapp.com.co",
                        UserName = "admin@llanoapp.com.co"
                    };

                    var user = _userManager.FindByEmailAsync(AdminUser.Email).Result;

                    if (user == null)
                    {
                        var createdUser = _userManager.CreateAsync(AdminUser, "1nt3rn3t*").Result;
                        var createdRol = _userManager.AddToRoleAsync(AdminUser, "Admin").Result;

                    }
                }
                catch (Exception ex) { }
            }
        }

    }
}
